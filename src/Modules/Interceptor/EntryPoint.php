<?php
/**
 * Created by PhpStorm.
 * User: greenx
 * Date: 04.03.19
 * Time: 14:21
 */

namespace App\Modules\Interceptor;

use App\Modules\Interceptor\Handler\CommonHandler;
use App\Modules\Interceptor\Serialize\MessageDecoderInterface;
use App\Modules\Interceptor\Validator\ValidatorInterface;
use Doctrine\ORM\EntityManagerInterface;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use OldSound\RabbitMqBundle\RabbitMq\Exception\StopConsumerException;
use PhpAmqpLib\Message\AMQPMessage;
use Psr\Log\LoggerInterface;

/**
 * Входная точка для всех сообщений. В данном случае - реализация rabbitmq-bundle
 * Class EntryPoint
 * @package App\Inerceptor\EntryPoint
 */
class EntryPoint implements ConsumerInterface
{
    /**
     * @var MessageDecoderInterface
     */
    private $messageDecoder;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var bool
     */
    private $dieSignal = false;

    /**
     * @var CommonHandler
     */
    private $handler;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;


    /**
     * EntryPoint constructor.
     * @param MessageDecoderInterface $messageSerializer
     * @param ValidatorInterface $validator
     * @param CommonHandler $handler
     * @param LoggerInterface $logger
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        MessageDecoderInterface $messageSerializer,
        ValidatorInterface $validator,
        CommonHandler $handler,
        LoggerInterface $logger,
        EntityManagerInterface $entityManager
    ) {
        $this->messageDecoder = $messageSerializer;
        $this->validator = $validator;
        $this->logger = $logger;
        $this->handler = $handler;
        $this->entityManager = $entityManager;
    }

    /**
     * @param AMQPMessage $msg
     * @return mixed
     * @throws \Throwable
     */
    public function execute(AMQPMessage $msg)
    {
        if ($this->dieSignal) throw new StopConsumerException;

        $this->logger->info(
            \sprintf('Handler %s incoming message: %s',
                \get_class($this->handler), \json_encode($msg->getBody())
            )
        );

        try {
            // Decode message.
            // Используется стандартный сериализатор. Сообщения не ожидаются жирными и их не должно быть много,
            // скорость обработки не сильно важна, поэтому пренебрёг скоростью.
            // При желании переделать экземпляр MessageDecoderInterface подобно в отелях.


            $message = $this->messageDecoder->decode($msg->getBody());

            // Validate.
            //$this->validator->validate($message);

            // Handle.
            $this->handler->handle($message);

            // Log.
            $this->logger->info(
                \sprintf('The handler %s worked successfully', \get_class($this->handler))
            );
        } catch (\Exception $e) {
            // If any exception return nack
            $this->logger->error(
                \sprintf(
                    "An error occurred: %s. \r\n Trace: %s. \r\n Message: %s.",
                    $e->getMessage(),
                    $e->getTraceAsString(),
                    \json_encode($msg->getBody())
                )
            );
            $this->dieSignal = true;
        }

        // Чистим и закрываем соединение
        $this->entityManager->clear();
        $this->entityManager->getConnection()->close();

        $this->logger->info(\sprintf('Handler %s clear', \get_class($this->handler)));

        // Return ack always;
        return ConsumerInterface::MSG_ACK;
    }
}