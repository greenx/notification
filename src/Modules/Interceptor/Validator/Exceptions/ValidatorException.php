<?php
/**
 * Created by PhpStorm.
 * User: greenx
 * Date: 26.03.19
 * Time: 19:33
 */

namespace App\Modules\Interceptor\Validator\Exceptions;

class ValidatorException extends \InvalidArgumentException
{
    /**
     * ValidationException constructor.
     * @param array $errors
     */
    public function __construct(array $errors)
    {
        parent::__construct(
            \sprintf(
                "The request data is incorrect:\n%s", implode(PHP_EOL, $errors)
            )
        );
    }
}