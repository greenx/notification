<?php

namespace App\Modules\Interceptor\Validator;

interface ValidatorInterface
{
    /**
     * @param object $object
     */
    public function validate(object $object): void;
}
