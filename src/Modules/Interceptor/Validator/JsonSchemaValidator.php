<?php

namespace App\Modules\Interceptor\Validator;

use App\Modules\Interceptor\Validator\Exceptions\ValidatorException;
use xxx\InterceptorBundle\Contract\Common\Message;
use JsonSchema\Validator;

class JsonSchemaValidator implements ValidatorInterface
{
    /**
     * @var Validator
     */
    private $validator;

    /**
     * @var string
     */
    private $baseSchemaPath;

    /**
     * @param string $baseSchemaPath
     */
    public function __construct(string $baseSchemaPath)
    {
        $this->validator = new Validator();
        $this->baseSchemaPath = $baseSchemaPath;
    }

    /**
     * {@inheritdoc}
     */
    public function validate(object $object): void
    {
        $this->validator->reset();
        /** @var Message $object */
        $path = \sprintf('%s%s.json', $this->baseSchemaPath, $object->header->type);

        $this->validator->validate($object, (object)['$ref' => $path]);

        if (!$this->validator->isValid()) {
            $errors = [];
            foreach ($this->validator->getErrors() as $error) {
                $errors[] = \sprintf('[%s] %s', $error['property'], $error['message']);
            }
            throw new ValidatorException($errors);
        }
    }
}
