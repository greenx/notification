<?php
/**
 * Created by PhpStorm.
 * User: greenx
 * Date: 04.03.19
 * Time: 19:30
 */

namespace App\Modules\Interceptor\Handler\Common;

interface HandlerInterface
{
    /**
     * @param $message
     */
    public function handle($message): void;
}