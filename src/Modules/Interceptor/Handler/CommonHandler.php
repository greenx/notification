<?php

namespace App\Modules\Interceptor\Handler;

use App\Exception\CreateNotificationException;
use App\Modules\Interceptor\Handler\Common\HandlerInterface;
use App\Service\Message\MessageService;
use Doctrine\ORM\EntityManagerInterface;
use xxx\NotificationAdminBundle\Entity\Event;


class CommonHandler implements HandlerInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var MessageService
     */
    private $messageService;

    /**
     * CommonHandler constructor.
     * @param EntityManagerInterface $entityManager
     * @param MessageService $messageService
     */
    public function __construct(EntityManagerInterface $entityManager, MessageService $messageService)
    {
        $this->entityManager = $entityManager;
        $this->messageService = $messageService;
    }

    /**
     * @param $message
     * @throws \Throwable
     */
    public function handle($message): void
    {
        if (false !== ($eventName = $message->header->meta->eventName)) {
            $events = $this->entityManager->getRepository(Event::class)->findBy(['event' => $eventName]);

            foreach ($events as $event) {
                try {
                    $this->messageService->send($event, $message->body);
                } catch (\Exception $exception) {
                    throw $exception;
                }
            }
        }

    }
}