<?php
/**
 * Created by PhpStorm.
 * User: greenx
 * Date: 04.03.19
 * Time: 19:42
 */

namespace App\Modules\Interceptor\Serialize;

use App\Modules\Interceptor\Serialize\Exceptions\NonexistentMessage;
use App\Modules\Interceptor\Mapper\Config\ConfigInterface;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer;
use Symfony\Component\PropertyInfo\Extractor\PhpDocExtractor;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Encoder\JsonDecode;

class MessageSerializer implements MessageDecoderInterface
{
    /**
     * @var ConfigInterface
     */
    private $mapper;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var JsonEncoder
     */
    private $encoder;

    /**
     * MessageSerializer constructor.
     * @param ConfigInterface $mapper
     * @param Serializer|null $serializer
     * @param array $normalizers
     * @param null $encoder
     */
    public function __construct(
       // ConfigInterface $mapper,
        Serializer $serializer = null,
        array $normalizers = [],
        $encoder = null
    ) {
        //$this->mapper = $mapper;

        $defaultNormalizers = [
            new Normalizer\DateTimeNormalizer(),
            new Normalizer\ArrayDenormalizer(),
            new ObjectNormalizer(null, null, null, new PhpDocExtractor()),
        ];

        $normalizers = \array_merge($defaultNormalizers, $normalizers);
        $this->serializer = $serializer ?? new Serializer($normalizers, [ new JsonEncoder(null, new JsonDecode()) ]);
        $this->encoder = $encoder ?? new JsonEncoder();
    }

    /**
     * @param string $message
     * @return object
     * @throws ExceptionInterface
     */
    public function decode(string $message): object
    {
        /** @var array{header:array, body:array} $data */
       return $this->serializer->decode($message, JsonEncoder::FORMAT);
    }
}