<?php
/**
 * Created by PhpStorm.
 * User: greenx
 * Date: 06.03.19
 * Time: 11:35
 */

namespace App\Modules\Interceptor\Serialize\Exceptions;

class NonexistentMessage extends \RuntimeException
{
    /**
     * NonexistentMessage constructor.
     * @param string $message
     */
    public function __construct(string $message)
    {
        parent::__construct(
            \sprintf(
                'The message %s does not exist on the config', $message
            )
        );
    }
}