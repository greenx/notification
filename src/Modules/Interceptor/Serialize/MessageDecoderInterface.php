<?php
/**
 * Created by PhpStorm.
 * User: greenx
 * Date: 05.03.19
 * Time: 23:00
 */

namespace App\Modules\Interceptor\Serialize;

/**
 * Декодер входящего сообщения.
 * Сейчас реализация через Serializer Symfony. Скорость обработки сообщений не сильно важна и
 * входящих сообщений в Заказах будет не очень много. Поэтому скоростью десериализации можно пренебречь.
 * Interface MessageDecoderInterface
 * @package App\Inerceptor\Serialize
 */
interface MessageDecoderInterface
{
    /**
     * @param string $message
     * @return object
     */
    public function decode(string $message): object;
}