<?php

namespace App\EntityListeners;

use App\Entity\Notification;
use App\Entity\NotificationLog;
use Doctrine\ORM\Event\LifecycleEventArgs;


class NotificationHistoryListener
{
    private $notificationLog;

    /**
     * @param LifecycleEventArgs $args
     */
    public function preUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if ($args->getObject() instanceof Notification) {
            /** @var $entity Notification */
            $this->notificationLog = new NotificationLog();
            $this->notificationLog->setTransportStatus($args->getOldValue('transportStatus'));
            $this->notificationLog->setNotification($entity);
            $this->notificationLog->setAccount($entity->getAccount());
        }
    }

    /*
     * @param LifecycleEventArgs $args
     */
    public function postUpdate(LifecycleEventArgs $args)
    {
        $entityManager = $args->getEntityManager();
        $entityManager->persist($this->notificationLog);
        $entityManager->flush();
    }
}