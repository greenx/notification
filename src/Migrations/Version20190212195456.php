<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190212195456 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA xxx_user');
        $this->addSql('CREATE TABLE xxx_user.account_property (id BIGSERIAL NOT NULL, account_id INT DEFAULT NULL, code VARCHAR(64) NOT NULL, value VARCHAR(1024) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_578A63259B6B5FBA ON xxx_user.account_property (account_id)');
        $this->addSql('CREATE TABLE xxx_user.contract_manage (id SERIAL NOT NULL, legal_id INT NOT NULL, code_settings VARCHAR(255) CHECK(code_settings IN (\'cash_vat\', \'cash_not_vat\', \'cashless_vat\', \'cashless_not_vat\', \'cash_sng\', \'cash_ru\', \'cash_foreign\', \'cashless_sng\', \'cashless_ru\', \'cashless_foreign\', \'ticket_cash_margin\', \'ticket_cash_not_margin\', \'ticket_cashless_margin\')) NOT NULL, partner VARCHAR(255) NOT NULL, enterprise VARCHAR(255) DEFAULT NULL, contract VARCHAR(255) DEFAULT NULL, order_currency_contract INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_7F6B6F6162BB3C59 ON xxx_user.contract_manage (legal_id)');
        $this->addSql('COMMENT ON COLUMN xxx_user.contract_manage.code_settings IS \'(DC2Type:ContractCodeSettingsType)\'');
        $this->addSql('CREATE TABLE xxx_user.account (id SERIAL NOT NULL, role_id INT DEFAULT NULL, legal_id INT DEFAULT NULL, name VARCHAR(100) NOT NULL, email VARCHAR(255) NOT NULL, phone VARCHAR(32) DEFAULT NULL, additional_phone VARCHAR(32) DEFAULT NULL, site VARCHAR(64) DEFAULT NULL, additional_info TEXT DEFAULT NULL, birthday TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, login VARCHAR(64) NOT NULL, password_md5 VARCHAR(64) NOT NULL, password_hash VARCHAR(255) NOT NULL, created TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, trash BOOLEAN DEFAULT \'false\' NOT NULL, points_ready INT DEFAULT 0 NOT NULL, points_processing INT DEFAULT 0 NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8B4370A8E7927C74 ON xxx_user.account (email)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8B4370A8AA08CB10 ON xxx_user.account (login)');
        $this->addSql('CREATE INDEX IDX_8B4370A8D60322AC ON xxx_user.account (role_id)');
        $this->addSql('CREATE INDEX IDX_8B4370A862BB3C59 ON xxx_user.account (legal_id)');
        $this->addSql('CREATE TABLE xxx_user.legal (id SERIAL NOT NULL, template_organization_id INT DEFAULT NULL, travel_agency_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, is_external BOOLEAN DEFAULT \'true\' NOT NULL, trash BOOLEAN DEFAULT \'false\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_EF4280E45E237E06 ON xxx_user.legal (name)');
        $this->addSql('CREATE INDEX IDX_EF4280E4CF0F4B5A ON xxx_user.legal (template_organization_id)');
        $this->addSql('CREATE INDEX IDX_EF4280E4CB361F56 ON xxx_user.legal (travel_agency_id)');
        $this->addSql('CREATE TABLE xxx_user.legal_partner (id SERIAL NOT NULL, legal_id INT DEFAULT NULL, partner_id VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_22FBE56B62BB3C59 ON xxx_user.legal_partner (legal_id)');
        $this->addSql('CREATE UNIQUE INDEX legal_partner_unique_idx ON xxx_user.legal_partner (legal_id, partner_id)');
        $this->addSql('CREATE TABLE xxx_user.legal_property (id BIGSERIAL NOT NULL, legal_id INT DEFAULT NULL, code VARCHAR(64) NOT NULL, value VARCHAR(1024) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_A555C18462BB3C59 ON xxx_user.legal_property (legal_id)');
        $this->addSql('CREATE TABLE xxx_user.role (id SERIAL NOT NULL, legal_id INT DEFAULT NULL, template_role_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, trash BOOLEAN DEFAULT \'false\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C13206A15E237E06 ON xxx_user.role (name)');
        $this->addSql('CREATE INDEX IDX_C13206A162BB3C59 ON xxx_user.role (legal_id)');
        $this->addSql('CREATE INDEX IDX_C13206A185BB5995 ON xxx_user.role (template_role_id)');
        $this->addSql('CREATE TABLE xxx_user.role_acl (id BIGSERIAL NOT NULL, role_id INT DEFAULT NULL, code VARCHAR(64) NOT NULL, value SMALLINT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_1A2D06DD60322AC ON xxx_user.role_acl (role_id)');
        $this->addSql('CREATE TABLE xxx_user.template_organization (id SERIAL NOT NULL, name VARCHAR(255) NOT NULL, is_admin BOOLEAN DEFAULT \'false\' NOT NULL, can_sign_up_on_site BOOLEAN DEFAULT \'false\' NOT NULL, is_xml_client BOOLEAN DEFAULT \'false\' NOT NULL, email_account_add VARCHAR(255) DEFAULT NULL, email_on_registration VARCHAR(255) DEFAULT NULL, email_pass_change VARCHAR(255) DEFAULT NULL, managers_by_default BOOLEAN DEFAULT \'false\' NOT NULL, is_foreign BOOLEAN DEFAULT \'false\' NOT NULL, trash BOOLEAN DEFAULT \'false\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6FCB61E75E237E06 ON xxx_user.template_organization (name)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6FCB61E7AB21560E ON xxx_user.template_organization (email_account_add)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6FCB61E753C459B4 ON xxx_user.template_organization (email_on_registration)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6FCB61E772B95F58 ON xxx_user.template_organization (email_pass_change)');
        $this->addSql('CREATE TABLE xxx_user.template_role (id SERIAL NOT NULL, name VARCHAR(255) NOT NULL, trash BOOLEAN DEFAULT \'false\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_564CE5145E237E06 ON xxx_user.template_role (name)');
        $this->addSql('CREATE TABLE xxx_user.template_role_template_organization (template_role_id INT NOT NULL, template_organization_id INT NOT NULL, PRIMARY KEY(template_role_id, template_organization_id))');
        $this->addSql('CREATE INDEX IDX_D183D0A685BB5995 ON xxx_user.template_role_template_organization (template_role_id)');
        $this->addSql('CREATE INDEX IDX_D183D0A6CF0F4B5A ON xxx_user.template_role_template_organization (template_organization_id)');
        $this->addSql('CREATE TABLE xxx_user.template_role_acl (id BIGSERIAL NOT NULL, template_role_id INT DEFAULT NULL, code VARCHAR(64) NOT NULL, value SMALLINT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_5F7D52A385BB5995 ON xxx_user.template_role_acl (template_role_id)');
        $this->addSql('CREATE TABLE xxx_user.template_organization_permit_property (id BIGSERIAL NOT NULL, template_organization_id INT DEFAULT NULL, code VARCHAR(2048) NOT NULL, value BOOLEAN DEFAULT \'true\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_988E911DCF0F4B5A ON xxx_user.template_organization_permit_property (template_organization_id)');
        $this->addSql('CREATE TABLE xxx_user.template_organization_property (id BIGSERIAL NOT NULL, template_organization_id INT DEFAULT NULL, code VARCHAR(64) NOT NULL, value VARCHAR(1024) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_D09C6347CF0F4B5A ON xxx_user.template_organization_property (template_organization_id)');
        $this->addSql('CREATE TABLE xxx_user.travel_agency (id SERIAL NOT NULL, name VARCHAR(100) NOT NULL, trash BOOLEAN DEFAULT \'false\' NOT NULL, category VARCHAR(255) CHECK(category IN (\'A\', \'B\', \'C\')) DEFAULT \'A\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_868139CB5E237E06 ON xxx_user.travel_agency (name)');
        $this->addSql('COMMENT ON COLUMN xxx_user.travel_agency.category IS \'(DC2Type:CategoryTravelAgencyType)\'');
        $this->addSql('ALTER TABLE xxx_user.account_property ADD CONSTRAINT FK_578A63259B6B5FBA FOREIGN KEY (account_id) REFERENCES xxx_user.account (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE xxx_user.contract_manage ADD CONSTRAINT FK_7F6B6F6162BB3C59 FOREIGN KEY (legal_id) REFERENCES xxx_user.legal (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE xxx_user.account ADD CONSTRAINT FK_8B4370A8D60322AC FOREIGN KEY (role_id) REFERENCES xxx_user.role (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE xxx_user.account ADD CONSTRAINT FK_8B4370A862BB3C59 FOREIGN KEY (legal_id) REFERENCES xxx_user.legal (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE xxx_user.legal ADD CONSTRAINT FK_EF4280E4CF0F4B5A FOREIGN KEY (template_organization_id) REFERENCES xxx_user.template_organization (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE xxx_user.legal ADD CONSTRAINT FK_EF4280E4CB361F56 FOREIGN KEY (travel_agency_id) REFERENCES xxx_user.travel_agency (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE xxx_user.legal_partner ADD CONSTRAINT FK_22FBE56B62BB3C59 FOREIGN KEY (legal_id) REFERENCES xxx_user.legal (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE xxx_user.legal_property ADD CONSTRAINT FK_A555C18462BB3C59 FOREIGN KEY (legal_id) REFERENCES xxx_user.legal (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE xxx_user.role ADD CONSTRAINT FK_C13206A162BB3C59 FOREIGN KEY (legal_id) REFERENCES xxx_user.legal (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE xxx_user.role ADD CONSTRAINT FK_C13206A185BB5995 FOREIGN KEY (template_role_id) REFERENCES xxx_user.template_role (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE xxx_user.role_acl ADD CONSTRAINT FK_1A2D06DD60322AC FOREIGN KEY (role_id) REFERENCES xxx_user.role (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE xxx_user.template_role_template_organization ADD CONSTRAINT FK_D183D0A685BB5995 FOREIGN KEY (template_role_id) REFERENCES xxx_user.template_role (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE xxx_user.template_role_template_organization ADD CONSTRAINT FK_D183D0A6CF0F4B5A FOREIGN KEY (template_organization_id) REFERENCES xxx_user.template_organization (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE xxx_user.template_role_acl ADD CONSTRAINT FK_5F7D52A385BB5995 FOREIGN KEY (template_role_id) REFERENCES xxx_user.template_role (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE xxx_user.template_organization_permit_property ADD CONSTRAINT FK_988E911DCF0F4B5A FOREIGN KEY (template_organization_id) REFERENCES xxx_user.template_organization (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE xxx_user.template_organization_property ADD CONSTRAINT FK_D09C6347CF0F4B5A FOREIGN KEY (template_organization_id) REFERENCES xxx_user.template_organization (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('CREATE SCHEMA event');
        $this->addSql('ALTER TABLE xxx_user.account_property DROP CONSTRAINT FK_578A63259B6B5FBA');
        $this->addSql('ALTER TABLE xxx_user.contract_manage DROP CONSTRAINT FK_7F6B6F6162BB3C59');
        $this->addSql('ALTER TABLE xxx_user.account DROP CONSTRAINT FK_8B4370A862BB3C59');
        $this->addSql('ALTER TABLE xxx_user.legal_partner DROP CONSTRAINT FK_22FBE56B62BB3C59');
        $this->addSql('ALTER TABLE xxx_user.legal_property DROP CONSTRAINT FK_A555C18462BB3C59');
        $this->addSql('ALTER TABLE xxx_user.role DROP CONSTRAINT FK_C13206A162BB3C59');
        $this->addSql('ALTER TABLE xxx_user.account DROP CONSTRAINT FK_8B4370A8D60322AC');
        $this->addSql('ALTER TABLE xxx_user.role_acl DROP CONSTRAINT FK_1A2D06DD60322AC');
        $this->addSql('ALTER TABLE xxx_user.legal DROP CONSTRAINT FK_EF4280E4CF0F4B5A');
        $this->addSql('ALTER TABLE xxx_user.template_role_template_organization DROP CONSTRAINT FK_D183D0A6CF0F4B5A');
        $this->addSql('ALTER TABLE xxx_user.template_organization_permit_property DROP CONSTRAINT FK_988E911DCF0F4B5A');
        $this->addSql('ALTER TABLE xxx_user.template_organization_property DROP CONSTRAINT FK_D09C6347CF0F4B5A');
        $this->addSql('ALTER TABLE xxx_user.role DROP CONSTRAINT FK_C13206A185BB5995');
        $this->addSql('ALTER TABLE xxx_user.template_role_template_organization DROP CONSTRAINT FK_D183D0A685BB5995');
        $this->addSql('ALTER TABLE xxx_user.template_role_acl DROP CONSTRAINT FK_5F7D52A385BB5995');
        $this->addSql('ALTER TABLE xxx_user.legal DROP CONSTRAINT FK_EF4280E4CB361F56');
        $this->addSql('DROP TABLE xxx_user.account_property');
        $this->addSql('DROP TABLE xxx_user.contract_manage');
        $this->addSql('DROP TABLE xxx_user.account');
        $this->addSql('DROP TABLE xxx_user.legal');
        $this->addSql('DROP TABLE xxx_user.legal_partner');
        $this->addSql('DROP TABLE xxx_user.legal_property');
        $this->addSql('DROP TABLE xxx_user.role');
        $this->addSql('DROP TABLE xxx_user.role_acl');
        $this->addSql('DROP TABLE xxx_user.template_organization');
        $this->addSql('DROP TABLE xxx_user.template_role');
        $this->addSql('DROP TABLE xxx_user.template_role_template_organization');
        $this->addSql('DROP TABLE xxx_user.template_role_acl');
        $this->addSql('DROP TABLE xxx_user.template_organization_permit_property');
        $this->addSql('DROP TABLE xxx_user.template_organization_property');
        $this->addSql('DROP TABLE xxx_user.travel_agency');
    }
}
