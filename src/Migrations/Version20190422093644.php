<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190422093644 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE xxx_user.contract_template (id SERIAL NOT NULL, name VARCHAR(255) NOT NULL, file VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE notification.notification_log DROP CONSTRAINT FK_67C7183FEF1A9D84');
        $this->addSql('ALTER TABLE notification.notification_log ADD CONSTRAINT FK_67C7183FEF1A9D84 FOREIGN KEY (notification_id) REFERENCES notification.notification (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE xxx_user.account_property ALTER account_id SET NOT NULL');
        $this->addSql('ALTER TABLE xxx_user.account_property ALTER value DROP NOT NULL');
        $this->addSql('ALTER TABLE xxx_user.account ALTER role_id SET NOT NULL');
        $this->addSql('ALTER TABLE xxx_user.account ALTER legal_id SET NOT NULL');
        $this->addSql('ALTER TABLE xxx_user.legal ALTER template_organization_id SET NOT NULL');
        $this->addSql('ALTER TABLE xxx_user.legal_property ALTER legal_id SET NOT NULL');
        $this->addSql('ALTER TABLE xxx_user.role ALTER legal_id SET NOT NULL');
        $this->addSql('ALTER TABLE xxx_user.role_acl ALTER role_id SET NOT NULL');
        $this->addSql('ALTER TABLE xxx_user.template_organization ADD emergency_email VARCHAR(1024) DEFAULT NULL');
        $this->addSql('ALTER TABLE xxx_user.template_role_acl ALTER template_role_id SET NOT NULL');
        $this->addSql('ALTER TABLE xxx_user.template_organization_permit_property ALTER template_organization_id SET NOT NULL');
        $this->addSql('ALTER TABLE xxx_user.template_organization_property ALTER template_organization_id SET NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('CREATE SCHEMA event');
        $this->addSql('CREATE SCHEMA xxx_security');
        $this->addSql('CREATE SCHEMA appnotification');
        $this->addSql('DROP TABLE xxx_user.contract_template');
        $this->addSql('ALTER TABLE xxx_user.legal ALTER template_organization_id DROP NOT NULL');
        $this->addSql('ALTER TABLE xxx_user.account_property ALTER account_id DROP NOT NULL');
        $this->addSql('ALTER TABLE xxx_user.account_property ALTER value SET NOT NULL');
        $this->addSql('ALTER TABLE xxx_user.role ALTER legal_id DROP NOT NULL');
        $this->addSql('ALTER TABLE xxx_user.legal_property ALTER legal_id DROP NOT NULL');
        $this->addSql('ALTER TABLE xxx_user.template_organization DROP emergency_email');
        $this->addSql('ALTER TABLE xxx_user.template_organization_permit_property ALTER template_organization_id DROP NOT NULL');
        $this->addSql('ALTER TABLE xxx_user.account ALTER role_id DROP NOT NULL');
        $this->addSql('ALTER TABLE xxx_user.account ALTER legal_id DROP NOT NULL');
        $this->addSql('ALTER TABLE notification.notification_log DROP CONSTRAINT fk_67c7183fef1a9d84');
        $this->addSql('ALTER TABLE notification.notification_log ADD CONSTRAINT fk_67c7183fef1a9d84 FOREIGN KEY (notification_id) REFERENCES notification.notification (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE xxx_user.role_acl ALTER role_id DROP NOT NULL');
        $this->addSql('ALTER TABLE xxx_user.template_organization_property ALTER template_organization_id DROP NOT NULL');
        $this->addSql('ALTER TABLE xxx_user.template_role_acl ALTER template_role_id DROP NOT NULL');
    }
}
