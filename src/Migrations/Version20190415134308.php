<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190415134308 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE xxx_user.account_property_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE xxx_user.legal_property_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE xxx_user.template_organization_permit_property_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE xxx_user.template_organization_property_id_seq CASCADE');

        $this->addSql('ALTER TABLE xxx_user.account_property ALTER id TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE xxx_user.account_property ALTER id TYPE UUID USING id::uuid');
        $this->addSql('ALTER TABLE xxx_user.account_property ALTER id DROP DEFAULT');
        $this->addSql('COMMENT ON COLUMN xxx_user.account_property.id IS \'(DC2Type:uuid)\'');

        $this->addSql('ALTER TABLE xxx_user.legal_property ALTER id TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE xxx_user.legal_property ALTER id TYPE UUID USING id::uuid');
        $this->addSql('ALTER TABLE xxx_user.legal_property ALTER id DROP DEFAULT');
        $this->addSql('COMMENT ON COLUMN xxx_user.legal_property.id IS \'(DC2Type:uuid)\'');

        $this->addSql('ALTER TABLE xxx_user.template_organization DROP email_account_add');
        $this->addSql('ALTER TABLE xxx_user.template_organization DROP email_on_registration');
        $this->addSql('ALTER TABLE xxx_user.template_organization DROP email_pass_change');

        $this->addSql('ALTER TABLE xxx_user.template_organization_permit_property ALTER id TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE xxx_user.template_organization_permit_property ALTER id TYPE UUID USING id::uuid');
        $this->addSql('ALTER TABLE xxx_user.template_organization_permit_property ALTER id DROP DEFAULT');
        $this->addSql('COMMENT ON COLUMN xxx_user.template_organization_permit_property.id IS \'(DC2Type:uuid)\'');

        $this->addSql('ALTER TABLE xxx_user.template_organization_property ALTER id TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE xxx_user.template_organization_property ALTER id TYPE UUID USING id::uuid');
        $this->addSql('ALTER TABLE xxx_user.template_organization_property ALTER id DROP DEFAULT');
        $this->addSql('COMMENT ON COLUMN xxx_user.template_organization_property.id IS \'(DC2Type:uuid)\'');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE xxx_user.account_property_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE xxx_user.legal_property_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE xxx_user.template_organization_permit_property_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE xxx_user.template_organization_property_id_seq INCREMENT BY 1 MINVALUE 1 START 1');

        $this->addSql('ALTER TABLE xxx_user.account_property ALTER id TYPE BIGINT USING id::varchar::bigint');
        $this->addSql('ALTER TABLE xxx_user.account_property ALTER id DROP DEFAULT');
        $this->addSql('SELECT setval(\'xxx_user.account_property_id_seq\', (SELECT MAX(id) FROM xxx_user.account_property))');
        $this->addSql('ALTER TABLE xxx_user.account_property ALTER id SET DEFAULT nextval(\'xxx_user.account_property_id_seq\')');
        $this->addSql('COMMENT ON COLUMN xxx_user.account_property.id IS NULL');

        $this->addSql('ALTER TABLE xxx_user.legal_property ALTER id TYPE BIGINT USING id::varchar::bigint');
        $this->addSql('ALTER TABLE xxx_user.legal_property ALTER id DROP DEFAULT');
        $this->addSql('SELECT setval(\'xxx_user.legal_property_id_seq\', (SELECT MAX(id) FROM xxx_user.legal_property))');
        $this->addSql('ALTER TABLE xxx_user.legal_property ALTER id SET DEFAULT nextval(\'xxx_user.legal_property_id_seq\')');
        $this->addSql('COMMENT ON COLUMN xxx_user.legal_property.id IS NULL');

        $this->addSql('ALTER TABLE xxx_user.template_organization_permit_property ALTER id TYPE BIGINT USING id::varchar::bigint');
        $this->addSql('ALTER TABLE xxx_user.template_organization_permit_property ALTER id DROP DEFAULT');
        $this->addSql('SELECT setval(\'xxx_user.template_organization_permit_property_id_seq\', (SELECT MAX(id) FROM xxx_user.template_organization_permit_property))');
        $this->addSql('ALTER TABLE xxx_user.template_organization_permit_property ALTER id SET DEFAULT nextval(\'xxx_user.template_organization_permit_property_id_seq\')');
        $this->addSql('COMMENT ON COLUMN xxx_user.template_organization_permit_property.id IS NULL');

        $this->addSql('ALTER TABLE xxx_user.template_organization_property ALTER id TYPE BIGINT USING id::varchar::bigint');
        $this->addSql('ALTER TABLE xxx_user.template_organization_property ALTER id DROP DEFAULT');
        $this->addSql('SELECT setval(\'xxx_user.template_organization_property_id_seq\', (SELECT MAX(id) FROM xxx_user.template_organization_property))');
        $this->addSql('ALTER TABLE xxx_user.template_organization_property ALTER id SET DEFAULT nextval(\'xxx_user.template_organization_property_id_seq\')');
        $this->addSql('COMMENT ON COLUMN xxx_user.template_organization_property.id IS NULL');

        $this->addSql('ALTER TABLE xxx_user.template_organization ADD email_account_add VARCHAR(1024) DEFAULT NULL');
        $this->addSql('ALTER TABLE xxx_user.template_organization ADD email_on_registration VARCHAR(1024) DEFAULT NULL');
        $this->addSql('ALTER TABLE xxx_user.template_organization ADD email_pass_change VARCHAR(1024) DEFAULT NULL');
    }
}
