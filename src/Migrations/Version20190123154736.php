<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190123154736 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA appnotification');
        $this->addSql('ALTER TABLE notification_log DROP CONSTRAINT fk_ed15df2ef1a9d84');
        $this->addSql('ALTER TABLE recipient DROP CONSTRAINT fk_6804fb4971f7e88b');
        $this->addSql('ALTER TABLE event.translations DROP CONSTRAINT fk_4cc67575232d562b');
        $this->addSql('ALTER TABLE notification DROP CONSTRAINT fk_bf5476ca71f7e88b');
        $this->addSql('DROP SEQUENCE notification_log_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE event.translations_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE event.event_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE appnotification.notification_log_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE appnotification.event (id SERIAL NOT NULL, event VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, subject VARCHAR(255) NOT NULL, message TEXT NOT NULL, transport VARCHAR(255) CHECK(transport IN (\'MAIL\', \'SMS\', \'PUSH\')) NOT NULL, application VARCHAR(255) CHECK(application IN (\'HOTEL\', \'ORDER\', \'OFFLINE_ORDER\')) NOT NULL, message_type VARCHAR(255) CHECK(message_type IN (\'PLAIN\', \'HTML\')) NOT NULL, json_schema_url VARCHAR(255) DEFAULT NULL, item_id_template VARCHAR(255) DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_50842A453BAE0AA7 ON appnotification.event (event)');
        $this->addSql('COMMENT ON COLUMN appnotification.event.transport IS \'(DC2Type:Transport)\'');
        $this->addSql('COMMENT ON COLUMN appnotification.event.application IS \'(DC2Type:Application)\'');
        $this->addSql('COMMENT ON COLUMN appnotification.event.message_type IS \'(DC2Type:MessageType)\'');
        $this->addSql('CREATE TABLE appnotification.event_translations (id SERIAL NOT NULL, object_id INT DEFAULT NULL, locale VARCHAR(8) NOT NULL, field VARCHAR(32) NOT NULL, content TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_B064695A232D562B ON appnotification.event_translations (object_id)');
        $this->addSql('CREATE UNIQUE INDEX event_translation_unique_idx ON appnotification.event_translations (locale, object_id, field)');
        $this->addSql('CREATE TABLE appnotification.notification (id SERIAL NOT NULL, event_id INT NOT NULL, account_id INT DEFAULT NULL, item_id INT NOT NULL, recipient VARCHAR(100) NOT NULL, subject VARCHAR(255) NOT NULL, message TEXT NOT NULL, transport_status VARCHAR(255) CHECK(transport_status IN (\'NEW\', \'SENT\', \'DELIVERED\', \'ERROR\')) NOT NULL, transport VARCHAR(255) CHECK(transport IN (\'MAIL\', \'SMS\', \'PUSH\')) NOT NULL, message_type VARCHAR(255) CHECK(message_type IN (\'PLAIN\', \'HTML\')) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_4B91836571F7E88B ON appnotification.notification (event_id)');
        $this->addSql('CREATE INDEX IDX_4B9183659B6B5FBA ON appnotification.notification (account_id)');
        $this->addSql('COMMENT ON COLUMN appnotification.notification.transport_status IS \'(DC2Type:TransportStatus)\'');
        $this->addSql('COMMENT ON COLUMN appnotification.notification.transport IS \'(DC2Type:Transport)\'');
        $this->addSql('COMMENT ON COLUMN appnotification.notification.message_type IS \'(DC2Type:MessageType)\'');
        $this->addSql('CREATE TABLE appnotification.notification_log (id INT NOT NULL, notification_id INT NOT NULL, account_id INT DEFAULT NULL, transport_status VARCHAR(255) CHECK(transport_status IN (\'NEW\', \'SENT\', \'DELIVERED\', \'ERROR\')) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_A48BF094EF1A9D84 ON appnotification.notification_log (notification_id)');
        $this->addSql('CREATE INDEX IDX_A48BF0949B6B5FBA ON appnotification.notification_log (account_id)');
        $this->addSql('COMMENT ON COLUMN appnotification.notification_log.transport_status IS \'(DC2Type:TransportStatus)\'');
        $this->addSql('CREATE TABLE appnotification.recipient (id SERIAL NOT NULL, event_id INT NOT NULL, argument VARCHAR(255) NOT NULL, recipient_type VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_993440E271F7E88B ON appnotification.recipient (event_id)');
        $this->addSql('ALTER TABLE appnotification.event_translations ADD CONSTRAINT FK_B064695A232D562B FOREIGN KEY (object_id) REFERENCES appnotification.event (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE appnotification.notification ADD CONSTRAINT FK_4B91836571F7E88B FOREIGN KEY (event_id) REFERENCES appnotification.event (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE appnotification.notification ADD CONSTRAINT FK_4B9183659B6B5FBA FOREIGN KEY (account_id) REFERENCES xxx_security.account (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE appnotification.notification_log ADD CONSTRAINT FK_A48BF094EF1A9D84 FOREIGN KEY (notification_id) REFERENCES appnotification.notification (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE appnotification.notification_log ADD CONSTRAINT FK_A48BF0949B6B5FBA FOREIGN KEY (account_id) REFERENCES xxx_security.account (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE appnotification.recipient ADD CONSTRAINT FK_993440E271F7E88B FOREIGN KEY (event_id) REFERENCES appnotification.event (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE recipient');
        $this->addSql('DROP TABLE event.translations');
        $this->addSql('DROP TABLE notification');
        $this->addSql('DROP TABLE notification_log');
        $this->addSql('DROP TABLE event.event');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('CREATE SCHEMA event');
        $this->addSql('ALTER TABLE appnotification.event_translations DROP CONSTRAINT FK_B064695A232D562B');
        $this->addSql('ALTER TABLE appnotification.notification DROP CONSTRAINT FK_4B91836571F7E88B');
        $this->addSql('ALTER TABLE appnotification.recipient DROP CONSTRAINT FK_993440E271F7E88B');
        $this->addSql('ALTER TABLE appnotification.notification_log DROP CONSTRAINT FK_A48BF094EF1A9D84');
        $this->addSql('DROP SEQUENCE appnotification.notification_log_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE notification_log_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE event.translations_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE event.event_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE recipient (id SERIAL NOT NULL, event_id INT NOT NULL, argument VARCHAR(255) NOT NULL, recipient_type VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_6804fb4971f7e88b ON recipient (event_id)');
        $this->addSql('CREATE TABLE event.translations (id SERIAL NOT NULL, object_id INT DEFAULT NULL, locale VARCHAR(8) NOT NULL, field VARCHAR(32) NOT NULL, content TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_4cc67575232d562b ON event.translations (object_id)');
        $this->addSql('CREATE UNIQUE INDEX event_translation_unique_idx ON event.translations (locale, object_id, field)');
        $this->addSql('CREATE TABLE notification (id SERIAL NOT NULL, event_id INT NOT NULL, account_id INT DEFAULT NULL, item_id INT NOT NULL, recipient VARCHAR(100) NOT NULL, subject VARCHAR(255) NOT NULL, message TEXT NOT NULL, transport_status VARCHAR(255) CHECK(transport_status IN (\'NEW\', \'SEND\', \'DELIVERED\', \'ERROR\')) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, transport VARCHAR(255) CHECK(transport IN (\'MAIL\', \'SMS\', \'PUSH\')) NOT NULL, message_type VARCHAR(255) CHECK(message_type IN (\'PLAIN\', \'HTML\')) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_bf5476ca71f7e88b ON notification (event_id)');
        $this->addSql('CREATE INDEX idx_bf5476ca9b6b5fba ON notification (account_id)');
        $this->addSql('COMMENT ON COLUMN notification.transport_status IS \'(DC2Type:TransportStatus)\'');
        $this->addSql('COMMENT ON COLUMN notification.transport IS \'(DC2Type:Transport)\'');
        $this->addSql('COMMENT ON COLUMN notification.message_type IS \'(DC2Type:MessageType)\'');
        $this->addSql('CREATE TABLE notification_log (id INT NOT NULL, notification_id INT NOT NULL, account_id INT DEFAULT NULL, transport_status VARCHAR(255) CHECK(transport_status IN (\'NEW\', \'SEND\', \'DELIVERED\', \'ERROR\')) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_ed15df2ef1a9d84 ON notification_log (notification_id)');
        $this->addSql('CREATE INDEX idx_ed15df29b6b5fba ON notification_log (account_id)');
        $this->addSql('COMMENT ON COLUMN notification_log.transport_status IS \'(DC2Type:TransportStatus)\'');
        $this->addSql('CREATE TABLE event.event (id SERIAL NOT NULL, event VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, subject VARCHAR(255) NOT NULL, message TEXT NOT NULL, transport VARCHAR(255) CHECK(transport IN (\'MAIL\', \'SMS\', \'PUSH\')) NOT NULL, application VARCHAR(255) CHECK(application IN (\'HOTEL\', \'ORDER\', \'OFFLINE_ORDER\')) NOT NULL, message_type VARCHAR(255) CHECK(message_type IN (\'PLAIN\', \'HTML\')) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, json_schema_url VARCHAR(255) DEFAULT NULL, item_id_template VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX uniq_bacb65ef3bae0aa7 ON event.event (event)');
        $this->addSql('COMMENT ON COLUMN event.event.transport IS \'(DC2Type:Transport)\'');
        $this->addSql('COMMENT ON COLUMN event.event.application IS \'(DC2Type:Application)\'');
        $this->addSql('COMMENT ON COLUMN event.event.message_type IS \'(DC2Type:MessageType)\'');
        $this->addSql('ALTER TABLE recipient ADD CONSTRAINT fk_6804fb4971f7e88b FOREIGN KEY (event_id) REFERENCES event.event (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE event.translations ADD CONSTRAINT fk_4cc67575232d562b FOREIGN KEY (object_id) REFERENCES event.event (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE notification ADD CONSTRAINT fk_bf5476ca9b6b5fba FOREIGN KEY (account_id) REFERENCES xxx_security.account (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE notification ADD CONSTRAINT fk_bf5476ca71f7e88b FOREIGN KEY (event_id) REFERENCES event.event (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE notification_log ADD CONSTRAINT fk_ed15df2ef1a9d84 FOREIGN KEY (notification_id) REFERENCES notification (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE notification_log ADD CONSTRAINT fk_ed15df29b6b5fba FOREIGN KEY (account_id) REFERENCES xxx_security.account (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE appnotification.event');
        $this->addSql('DROP TABLE appnotification.event_translations');
        $this->addSql('DROP TABLE appnotification.notification');
        $this->addSql('DROP TABLE appnotification.notification_log');
        $this->addSql('DROP TABLE appnotification.recipient');
    }
}
