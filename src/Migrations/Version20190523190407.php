<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190523190407 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE xxx_user.register_data ADD already_have_contract BOOLEAN DEFAULT \'false\' NOT NULL');
        $this->addSql('ALTER TABLE xxx_user.register_data ADD personal_data_agreement BOOLEAN DEFAULT \'false\' NOT NULL');
        $this->addSql('ALTER TABLE xxx_user.register_data ADD use_policy_agreement BOOLEAN DEFAULT \'false\' NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE xxx_user.register_data DROP already_have_contract');
        $this->addSql('ALTER TABLE xxx_user.register_data DROP personal_data_agreement');
        $this->addSql('ALTER TABLE xxx_user.register_data DROP use_policy_agreement');
    }
}
