<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190122181018 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE notification DROP CONSTRAINT notification_transport_status_check');
        $this->addSql('UPDATE notification SET transport_status = \'SEND\' WHERE transport_status=\'SENT\'');
        $this->addSql('ALTER TABLE notification ADD CONSTRAINT notification_transport_status_check CHECK(transport_status IN (\'NEW\', \'SEND\', \'DELIVERED\', \'READ\', \'ERROR\'))');
        $this->addSql('ALTER TABLE notification ALTER message_type DROP DEFAULT');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE notification DROP CONSTRAINT notification_transport_status_check');
        $this->addSql('UPDATE notification SET transport_status = \'SENT\' WHERE transport_status=\'SEND\'');
        $this->addSql('ALTER TABLE notification ADD CONSTRAINT notification_transport_status_check CHECK(transport_status IN (\'NEW\', \'SENT\', \'DELIVERED\', \'READ\', \'ERROR\'))');

        $this->addSql('ALTER TABLE notification ALTER message_type SET DEFAULT \'PLAIN\'');
    }
}
