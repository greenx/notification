<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190519093119 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE xxx_user.legal ADD office_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE xxx_user.legal ADD CONSTRAINT FK_EF4280E4FFA0C224 FOREIGN KEY (office_id) REFERENCES xxx_user.office (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_EF4280E4FFA0C224 ON xxx_user.legal (office_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE xxx_user.legal DROP CONSTRAINT FK_EF4280E4FFA0C224');
        $this->addSql('DROP INDEX IDX_EF4280E4FFA0C224');
        $this->addSql('ALTER TABLE xxx_user.legal DROP office_id');
    }
}
