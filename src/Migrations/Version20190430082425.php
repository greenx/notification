<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190430082425 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE xxx_user.role_property (id UUID NOT NULL, role_id INT NOT NULL, code VARCHAR(64) NOT NULL, value VARCHAR(8192) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_75CABFFCD60322AC ON xxx_user.role_property (role_id)');
        $this->addSql('COMMENT ON COLUMN xxx_user.role_property.id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE xxx_user.template_role_property (id UUID NOT NULL, template_role_id INT NOT NULL, code VARCHAR(64) NOT NULL, value VARCHAR(4096) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_C652952185BB5995 ON xxx_user.template_role_property (template_role_id)');
        $this->addSql('COMMENT ON COLUMN xxx_user.template_role_property.id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE xxx_user.role_property ADD CONSTRAINT FK_75CABFFCD60322AC FOREIGN KEY (role_id) REFERENCES xxx_user.role (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE xxx_user.template_role_property ADD CONSTRAINT FK_C652952185BB5995 FOREIGN KEY (template_role_id) REFERENCES xxx_user.template_role (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE xxx_user.template_organization_permit_property ALTER code TYPE VARCHAR(64)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP TABLE xxx_user.role_property');
        $this->addSql('DROP TABLE xxx_user.template_role_property');
        $this->addSql('ALTER TABLE xxx_user.template_organization_permit_property ALTER code TYPE VARCHAR(2048)');
    }
}
