<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190528094106 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE xxx_user.register_data ADD CHECK(tax_system IN (\'GENERAL\', \'SIMPLIFIED\'))');
        $this->addSql('ALTER TABLE xxx_user.register_data ALTER tax_system DROP DEFAULT');
        $this->addSql('ALTER TABLE xxx_user.register_data ADD CHECK(founding_document IN (\'CHARTER\', \'POWER_OF_ATTORNEY\'))');
        $this->addSql('ALTER TABLE xxx_user.register_data ALTER founding_document DROP DEFAULT');
        $this->addSql('ALTER TABLE xxx_user.register_data ALTER inn TYPE VARCHAR(12)');
        $this->addSql('ALTER TABLE xxx_user.register_data ALTER kpp TYPE VARCHAR(9)');
        $this->addSql('ALTER TABLE xxx_user.register_data ALTER okpo TYPE VARCHAR(8)');
        $this->addSql('ALTER TABLE xxx_user.register_data ALTER bank_rs TYPE VARCHAR(20)');
        $this->addSql('ALTER TABLE xxx_user.register_data ALTER bank_cs TYPE VARCHAR(20)');
        $this->addSql('ALTER TABLE xxx_user.register_data ALTER bank_bic TYPE VARCHAR(9)');
        $this->addSql('COMMENT ON COLUMN xxx_user.register_data.tax_system IS \'(DC2Type:TaxSystemType)\'');
        $this->addSql('COMMENT ON COLUMN xxx_user.register_data.founding_document IS \'(DC2Type:FoundingDocumentType)\'');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE xxx_user.register_data ALTER tax_system TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE xxx_user.register_data ALTER tax_system DROP DEFAULT');
        $this->addSql('ALTER TABLE xxx_user.register_data ALTER founding_document TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE xxx_user.register_data ALTER founding_document DROP DEFAULT');
        $this->addSql('ALTER TABLE xxx_user.register_data ALTER inn TYPE VARCHAR(24)');
        $this->addSql('ALTER TABLE xxx_user.register_data ALTER kpp TYPE VARCHAR(64)');
        $this->addSql('ALTER TABLE xxx_user.register_data ALTER okpo TYPE VARCHAR(64)');
        $this->addSql('ALTER TABLE xxx_user.register_data ALTER bank_rs TYPE VARCHAR(64)');
        $this->addSql('ALTER TABLE xxx_user.register_data ALTER bank_cs TYPE VARCHAR(64)');
        $this->addSql('ALTER TABLE xxx_user.register_data ALTER bank_bic TYPE VARCHAR(64)');
        $this->addSql('COMMENT ON COLUMN xxx_user.register_data.tax_system IS NULL');
        $this->addSql('COMMENT ON COLUMN xxx_user.register_data.founding_document IS NULL');
    }
}
