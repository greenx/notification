<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181229102319 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA event');
        $this->addSql('CREATE SCHEMA xxx_security');
        $this->addSql('CREATE SEQUENCE notification_log_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE xxx_security.contract_manage_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE event.translation (id SERIAL NOT NULL, event VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, subject VARCHAR(255) NOT NULL, message TEXT NOT NULL, transport VARCHAR(255) CHECK(transport IN (\'MAIL\', \'SMS\', \'PUSH\')) NOT NULL, application VARCHAR(255) CHECK(application IN (\'HOTEL\', \'ORDER\', \'OFFLINE_ORDER\')) NOT NULL, message_type VARCHAR(255) CHECK(message_type IN (\'PLAIN\', \'HTML\')) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_A00F5B743BAE0AA7 ON event.translation (event)');
        $this->addSql('COMMENT ON COLUMN event.translation.transport IS \'(DC2Type:TransportInterface)\'');
        $this->addSql('COMMENT ON COLUMN event.translation.application IS \'(DC2Type:Application)\'');
        $this->addSql('COMMENT ON COLUMN event.translation.message_type IS \'(DC2Type:MessageType)\'');
        $this->addSql('CREATE TABLE event.translations (id SERIAL NOT NULL, object_id INT DEFAULT NULL, locale VARCHAR(8) NOT NULL, field VARCHAR(32) NOT NULL, content TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_4CC67575232D562B ON event.translations (object_id)');
        $this->addSql('CREATE UNIQUE INDEX event_translation_unique_idx ON event.translations (locale, object_id, field)');
        $this->addSql('CREATE TABLE notification (id SERIAL NOT NULL, event_id INT NOT NULL, account_id INT DEFAULT NULL, item_id INT NOT NULL, recipient VARCHAR(100) NOT NULL, subject VARCHAR(255) NOT NULL, message TEXT NOT NULL, transport_status VARCHAR(255) CHECK(transport_status IN (\'NEW\', \'SENT\', \'DELIVERED\', \'READ\', \'ERROR\')) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_BF5476CA71F7E88B ON notification (event_id)');
        $this->addSql('CREATE INDEX IDX_BF5476CA9B6B5FBA ON notification (account_id)');
        $this->addSql('COMMENT ON COLUMN notification.transport_status IS \'(DC2Type:TransportStatus)\'');
        $this->addSql('CREATE TABLE notification_log (id INT NOT NULL, notification_id INT NOT NULL, account_id INT NOT NULL, transport_status VARCHAR(255) CHECK(transport_status IN (\'NEW\', \'SENT\', \'DELIVERED\', \'READ\', \'ERROR\')) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_ED15DF2EF1A9D84 ON notification_log (notification_id)');
        $this->addSql('CREATE INDEX IDX_ED15DF29B6B5FBA ON notification_log (account_id)');
        $this->addSql('COMMENT ON COLUMN notification_log.transport_status IS \'(DC2Type:TransportStatus)\'');
        $this->addSql('CREATE TABLE xxx_security.account_property (id SERIAL NOT NULL, account_id INT DEFAULT NULL, code VARCHAR(64) NOT NULL, value VARCHAR(1024) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_53A143E99B6B5FBA ON xxx_security.account_property (account_id)');
        $this->addSql('CREATE TABLE xxx_security.contract_manage (id INT NOT NULL, legal_id INT NOT NULL, code_settings VARCHAR(255) CHECK(code_settings IN (\'cash_vat\', \'cash_not_vat\', \'cashless_vat\', \'cashless_not_vat\', \'cash_sng\', \'cash_ru\', \'cash_foreign\', \'cashless_sng\', \'cashless_ru\', \'cashless_foreign\', \'ticket_cash_margin\', \'ticket_cash_not_margin\', \'ticket_cashless_margin\')) NOT NULL, partner VARCHAR(255) NOT NULL, enterprise VARCHAR(255) DEFAULT NULL, contract VARCHAR(255) DEFAULT NULL, order_currency_contract INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_8F6DB6E762BB3C59 ON xxx_security.contract_manage (legal_id)');
        $this->addSql('COMMENT ON COLUMN xxx_security.contract_manage.code_settings IS \'(DC2Type:ContractCodeSettingsType)\'');
        $this->addSql('CREATE TABLE xxx_security.legal (id INT NOT NULL, template_organization_id INT DEFAULT NULL, trash BOOLEAN DEFAULT \'false\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_A873A258CF0F4B5A ON xxx_security.legal (template_organization_id)');
        $this->addSql('CREATE TABLE xxx_security.legal_partner (id SERIAL NOT NULL, legal_id INT DEFAULT NULL, partner_db INT DEFAULT NULL, partner_id VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_DE755E6762BB3C59 ON xxx_security.legal_partner (legal_id)');
        $this->addSql('CREATE UNIQUE INDEX legal_partner_unique_idx ON xxx_security.legal_partner (legal_id, partner_id)');
        $this->addSql('CREATE TABLE xxx_security.legal_property (id SERIAL NOT NULL, legal_id INT DEFAULT NULL, code VARCHAR(64) NOT NULL, value VARCHAR(1024) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_AC1F031462BB3C59 ON xxx_security.legal_property (legal_id)');
        $this->addSql('CREATE TABLE xxx_security.role (id INT NOT NULL, legal_id INT DEFAULT NULL, template_role_id INT DEFAULT NULL, trash BOOLEAN DEFAULT \'false\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_42DFC54F62BB3C59 ON xxx_security.role (legal_id)');
        $this->addSql('CREATE INDEX IDX_42DFC54F85BB5995 ON xxx_security.role (template_role_id)');
        $this->addSql('CREATE TABLE xxx_security.role_acl (id SERIAL NOT NULL, role_id INT DEFAULT NULL, code VARCHAR(64) NOT NULL, value SMALLINT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_3B8A9144D60322AC ON xxx_security.role_acl (role_id)');
        $this->addSql('CREATE TABLE xxx_security.template_organization (id INT NOT NULL, trash BOOLEAN DEFAULT \'false\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE xxx_security.template_role (id INT NOT NULL, trash BOOLEAN DEFAULT \'false\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE xxx_security.template_role_template_organization (xxx_security_template_role_id INT NOT NULL, xxx_security_template_organization_id INT NOT NULL, PRIMARY KEY(xxx_security_template_role_id, xxx_security_template_organization_id))');
        $this->addSql('CREATE INDEX IDX_F40D26EE14A5E81E ON xxx_security.template_role_template_organization (xxx_security_template_role_id)');
        $this->addSql('CREATE INDEX IDX_F40D26EE92E6FB89 ON xxx_security.template_role_template_organization (xxx_security_template_organization_id)');
        $this->addSql('CREATE TABLE xxx_security.template_role_acl (id SERIAL NOT NULL, template_role_id INT DEFAULT NULL, code VARCHAR(64) NOT NULL, value SMALLINT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_CDABF71885BB5995 ON xxx_security.template_role_acl (template_role_id)');
        $this->addSql('CREATE TABLE xxx_security.template_organization_permit_property (id SERIAL NOT NULL, template_organization_id INT DEFAULT NULL, code VARCHAR(2048) NOT NULL, value BOOLEAN DEFAULT \'true\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_F4F026B7CF0F4B5A ON xxx_security.template_organization_permit_property (template_organization_id)');
        $this->addSql('CREATE TABLE xxx_security.template_organization_property (id SERIAL NOT NULL, template_organization_id INT DEFAULT NULL, code VARCHAR(64) NOT NULL, value VARCHAR(1024) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_52A7ADF8CF0F4B5A ON xxx_security.template_organization_property (template_organization_id)');
        $this->addSql('CREATE TABLE xxx_security.account (id INT NOT NULL, role_id INT DEFAULT NULL, legal_id INT DEFAULT NULL, trash BOOLEAN DEFAULT \'false\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_165397C9D60322AC ON xxx_security.account (role_id)');
        $this->addSql('CREATE INDEX IDX_165397C962BB3C59 ON xxx_security.account (legal_id)');
        $this->addSql('ALTER TABLE event.translations ADD CONSTRAINT FK_4CC67575232D562B FOREIGN KEY (object_id) REFERENCES event.translation (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE notification ADD CONSTRAINT FK_BF5476CA71F7E88B FOREIGN KEY (event_id) REFERENCES event.translation (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE notification ADD CONSTRAINT FK_BF5476CA9B6B5FBA FOREIGN KEY (account_id) REFERENCES xxx_security.account (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE notification_log ADD CONSTRAINT FK_ED15DF2EF1A9D84 FOREIGN KEY (notification_id) REFERENCES notification (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE notification_log ADD CONSTRAINT FK_ED15DF29B6B5FBA FOREIGN KEY (account_id) REFERENCES xxx_security.account (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE xxx_security.account_property ADD CONSTRAINT FK_53A143E99B6B5FBA FOREIGN KEY (account_id) REFERENCES xxx_security.account (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE xxx_security.contract_manage ADD CONSTRAINT FK_8F6DB6E762BB3C59 FOREIGN KEY (legal_id) REFERENCES xxx_security.legal (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE xxx_security.legal ADD CONSTRAINT FK_A873A258CF0F4B5A FOREIGN KEY (template_organization_id) REFERENCES xxx_security.template_organization (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE xxx_security.legal_partner ADD CONSTRAINT FK_DE755E6762BB3C59 FOREIGN KEY (legal_id) REFERENCES xxx_security.legal (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE xxx_security.legal_property ADD CONSTRAINT FK_AC1F031462BB3C59 FOREIGN KEY (legal_id) REFERENCES xxx_security.legal (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE xxx_security.role ADD CONSTRAINT FK_42DFC54F62BB3C59 FOREIGN KEY (legal_id) REFERENCES xxx_security.legal (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE xxx_security.role ADD CONSTRAINT FK_42DFC54F85BB5995 FOREIGN KEY (template_role_id) REFERENCES xxx_security.template_role (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE xxx_security.role_acl ADD CONSTRAINT FK_3B8A9144D60322AC FOREIGN KEY (role_id) REFERENCES xxx_security.role (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE xxx_security.template_role_template_organization ADD CONSTRAINT FK_F40D26EE14A5E81E FOREIGN KEY (xxx_security_template_role_id) REFERENCES xxx_security.template_role (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE xxx_security.template_role_template_organization ADD CONSTRAINT FK_F40D26EE92E6FB89 FOREIGN KEY (xxx_security_template_organization_id) REFERENCES xxx_security.template_organization (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE xxx_security.template_role_acl ADD CONSTRAINT FK_CDABF71885BB5995 FOREIGN KEY (template_role_id) REFERENCES xxx_security.template_role (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE xxx_security.template_organization_permit_property ADD CONSTRAINT FK_F4F026B7CF0F4B5A FOREIGN KEY (template_organization_id) REFERENCES xxx_security.template_organization (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE xxx_security.template_organization_property ADD CONSTRAINT FK_52A7ADF8CF0F4B5A FOREIGN KEY (template_organization_id) REFERENCES xxx_security.template_organization (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE xxx_security.account ADD CONSTRAINT FK_165397C9D60322AC FOREIGN KEY (role_id) REFERENCES xxx_security.role (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE xxx_security.account ADD CONSTRAINT FK_165397C962BB3C59 FOREIGN KEY (legal_id) REFERENCES xxx_security.legal (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE event.translations DROP CONSTRAINT FK_4CC67575232D562B');
        $this->addSql('ALTER TABLE notification DROP CONSTRAINT FK_BF5476CA71F7E88B');
        $this->addSql('ALTER TABLE notification_log DROP CONSTRAINT FK_ED15DF2EF1A9D84');
        $this->addSql('ALTER TABLE xxx_security.contract_manage DROP CONSTRAINT FK_8F6DB6E762BB3C59');
        $this->addSql('ALTER TABLE xxx_security.legal_partner DROP CONSTRAINT FK_DE755E6762BB3C59');
        $this->addSql('ALTER TABLE xxx_security.legal_property DROP CONSTRAINT FK_AC1F031462BB3C59');
        $this->addSql('ALTER TABLE xxx_security.role DROP CONSTRAINT FK_42DFC54F62BB3C59');
        $this->addSql('ALTER TABLE xxx_security.account DROP CONSTRAINT FK_165397C962BB3C59');
        $this->addSql('ALTER TABLE xxx_security.role_acl DROP CONSTRAINT FK_3B8A9144D60322AC');
        $this->addSql('ALTER TABLE xxx_security.account DROP CONSTRAINT FK_165397C9D60322AC');
        $this->addSql('ALTER TABLE xxx_security.legal DROP CONSTRAINT FK_A873A258CF0F4B5A');
        $this->addSql('ALTER TABLE xxx_security.template_role_template_organization DROP CONSTRAINT FK_F40D26EE92E6FB89');
        $this->addSql('ALTER TABLE xxx_security.template_organization_permit_property DROP CONSTRAINT FK_F4F026B7CF0F4B5A');
        $this->addSql('ALTER TABLE xxx_security.template_organization_property DROP CONSTRAINT FK_52A7ADF8CF0F4B5A');
        $this->addSql('ALTER TABLE xxx_security.role DROP CONSTRAINT FK_42DFC54F85BB5995');
        $this->addSql('ALTER TABLE xxx_security.template_role_template_organization DROP CONSTRAINT FK_F40D26EE14A5E81E');
        $this->addSql('ALTER TABLE xxx_security.template_role_acl DROP CONSTRAINT FK_CDABF71885BB5995');
        $this->addSql('ALTER TABLE notification DROP CONSTRAINT FK_BF5476CA9B6B5FBA');
        $this->addSql('ALTER TABLE notification_log DROP CONSTRAINT FK_ED15DF29B6B5FBA');
        $this->addSql('ALTER TABLE xxx_security.account_property DROP CONSTRAINT FK_53A143E99B6B5FBA');
        $this->addSql('DROP SEQUENCE notification_log_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE xxx_security.contract_manage_id_seq CASCADE');
        $this->addSql('DROP TABLE event.translation');
        $this->addSql('DROP TABLE event.translations');
        $this->addSql('DROP TABLE notification');
        $this->addSql('DROP TABLE notification_log');
        $this->addSql('DROP TABLE xxx_security.account_property');
        $this->addSql('DROP TABLE xxx_security.contract_manage');
        $this->addSql('DROP TABLE xxx_security.legal');
        $this->addSql('DROP TABLE xxx_security.legal_partner');
        $this->addSql('DROP TABLE xxx_security.legal_property');
        $this->addSql('DROP TABLE xxx_security.role');
        $this->addSql('DROP TABLE xxx_security.role_acl');
        $this->addSql('DROP TABLE xxx_security.template_organization');
        $this->addSql('DROP TABLE xxx_security.template_role');
        $this->addSql('DROP TABLE xxx_security.template_role_template_organization');
        $this->addSql('DROP TABLE xxx_security.template_role_acl');
        $this->addSql('DROP TABLE xxx_security.template_organization_permit_property');
        $this->addSql('DROP TABLE xxx_security.template_organization_property');
        $this->addSql('DROP TABLE xxx_security.account');
    }
}
