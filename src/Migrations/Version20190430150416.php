<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190430150416 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE xxx_user.contract_template ADD office_id INT NOT NULL');
        $this->addSql('ALTER TABLE xxx_user.contract_template ADD contract_type VARCHAR(255) CHECK(contract_type IN (\'AGENCY_CONTRACT_TYPE\', \'SERVICE_CONTRACT_TYPE\')) NOT NULL');
        $this->addSql('COMMENT ON COLUMN xxx_user.contract_template.contract_type IS \'(DC2Type:ContractType)\'');
        $this->addSql('ALTER TABLE xxx_user.contract_template ADD CONSTRAINT FK_A1E8FCCFFA0C224 FOREIGN KEY (office_id) REFERENCES xxx_user.office (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_A1E8FCCFFA0C224 ON xxx_user.contract_template (office_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE xxx_user.contract_template DROP CONSTRAINT FK_A1E8FCCFFA0C224');
        $this->addSql('DROP INDEX IDX_A1E8FCCFFA0C224');
        $this->addSql('ALTER TABLE xxx_user.contract_template DROP office_id');
        $this->addSql('ALTER TABLE xxx_user.contract_template DROP contract_type');
    }
}
