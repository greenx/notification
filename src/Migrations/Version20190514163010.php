<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190514163010 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE xxx_user.legal_property ADD parent_value VARCHAR(1024) DEFAULT NULL');
        $this->addSql('ALTER TABLE xxx_user.legal_property ADD by_parent BOOLEAN DEFAULT \'true\' NOT NULL');
        $this->addSql('ALTER TABLE xxx_user.role_acl ADD by_parent BOOLEAN DEFAULT \'true\' NOT NULL');
        $this->addSql('ALTER TABLE xxx_user.role_acl ALTER value DROP NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE xxx_user.legal_property DROP parent_value');
        $this->addSql('ALTER TABLE xxx_user.legal_property DROP by_parent');
        $this->addSql('ALTER TABLE xxx_user.role_acl DROP by_parent');
        $this->addSql('ALTER TABLE xxx_user.role_acl ALTER value SET NOT NULL');
    }
}
