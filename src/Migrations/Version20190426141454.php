<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190426141454 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA dictionary');
        $this->addSql('CREATE SEQUENCE dictionary.city_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE dictionary.location_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE dictionary.resort_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE dictionary.state_region_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE dictionary.underground_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE xxx_user.office (id SERIAL NOT NULL, title VARCHAR(255) NOT NULL, address VARCHAR(512) DEFAULT NULL, email VARCHAR(1024) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE xxx_user.office_city (office_id INT NOT NULL, city_id INT NOT NULL, PRIMARY KEY(office_id, city_id))');
        $this->addSql('CREATE INDEX IDX_B034F648FFA0C224 ON xxx_user.office_city (office_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_B034F6488BAC62AF ON xxx_user.office_city (city_id)');
        $this->addSql('CREATE TABLE dictionary.city (id INT NOT NULL, resort_id INT DEFAULT NULL, state_region_id INT DEFAULT NULL, country_id INT DEFAULT NULL, nameRu VARCHAR(255) NOT NULL, nameEn VARCHAR(255) NOT NULL, fromName VARCHAR(255) DEFAULT NULL, toName VARCHAR(255) DEFAULT NULL, inName VARCHAR(255) DEFAULT NULL, major BOOLEAN DEFAULT NULL, isSubCity BOOLEAN DEFAULT NULL, special BOOLEAN DEFAULT NULL, trash BOOLEAN NOT NULL, hasUnderground BOOLEAN DEFAULT NULL, hotelCount INT DEFAULT NULL, latitude DOUBLE PRECISION DEFAULT NULL, longitude DOUBLE PRECISION DEFAULT NULL, radius INT DEFAULT NULL, isCapital BOOLEAN DEFAULT NULL, isRegion BOOLEAN DEFAULT NULL, timezone TEXT DEFAULT NULL, regionCity_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_FCE83007A3ABE5D ON dictionary.city (resort_id)');
        $this->addSql('CREATE INDEX IDX_FCE8300D0ABC478 ON dictionary.city (state_region_id)');
        $this->addSql('CREATE INDEX IDX_FCE8300F92F3E70 ON dictionary.city (country_id)');
        $this->addSql('CREATE INDEX IDX_FCE8300AEF640E5 ON dictionary.city (regionCity_id)');
        $this->addSql('CREATE INDEX idx_city_name_ru ON dictionary.city (nameRu)');
        $this->addSql('CREATE INDEX idx_city_name_en ON dictionary.city (nameEn)');
        $this->addSql('CREATE INDEX idx_city_is_major ON dictionary.city (major)');
        $this->addSql('CREATE INDEX idx_city_is_special ON dictionary.city (special)');
        $this->addSql('CREATE INDEX idx_city_trash ON dictionary.city (trash)');
        $this->addSql('CREATE INDEX idx_city_name_ru_lower ON dictionary.city (lower(nameru) text_pattern_ops)');
        $this->addSql('CREATE INDEX idx_city_name_en_lower ON dictionary.city (lower(nameen) text_pattern_ops)');
        $this->addSql('CREATE TABLE dictionary.country (id SERIAL NOT NULL, region_id INT DEFAULT NULL, alpha2 VARCHAR(2) DEFAULT NULL, alpha3 VARCHAR(3) DEFAULT NULL, numeric INT DEFAULT NULL, nameEn VARCHAR(255) NOT NULL, nameRu VARCHAR(255) NOT NULL, isSng BOOLEAN NOT NULL, isShengen BOOLEAN NOT NULL, visaId INT DEFAULT NULL, t1_insurance BOOLEAN DEFAULT \'false\' NOT NULL, trash BOOLEAN NOT NULL, flag TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_D2EC4AC998260155 ON dictionary.country (region_id)');
        $this->addSql('CREATE INDEX idx_country_alpha2 ON dictionary.country (alpha2)');
        $this->addSql('CREATE INDEX idx_country_alpha3 ON dictionary.country (alpha3)');
        $this->addSql('CREATE INDEX idx_country_numeric ON dictionary.country (numeric)');
        $this->addSql('CREATE INDEX idx_country_name_ru ON dictionary.country (nameRu)');
        $this->addSql('CREATE INDEX idx_country_name_en ON dictionary.country (nameEn)');
        $this->addSql('CREATE INDEX idx_country_trash ON dictionary.country (trash)');
        $this->addSql('CREATE INDEX idx_country_t1_insurance ON dictionary.country (t1_insurance)');
        $this->addSql('COMMENT ON COLUMN dictionary.country.flag IS \'(DC2Type:array)\'');
        $this->addSql('CREATE TABLE dictionary.currency (id VARCHAR(3) NOT NULL, nameRu VARCHAR(16) NOT NULL, nameEn VARCHAR(16) NOT NULL, rate DOUBLE PRECISION NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_currency_name_ru ON dictionary.currency (nameRu)');
        $this->addSql('CREATE INDEX idx_currency_name_en ON dictionary.currency (nameEn)');
        $this->addSql('CREATE TABLE dictionary.location (id INT NOT NULL, city_id INT DEFAULT NULL, nameRu VARCHAR(255) NOT NULL, nameEn VARCHAR(255) NOT NULL, isGlobal BOOLEAN DEFAULT NULL, trash BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_1876A8318BAC62AF ON dictionary.location (city_id)');
        $this->addSql('CREATE INDEX idx_location_name_ru ON dictionary.location (nameRu)');
        $this->addSql('CREATE INDEX idx_location_name_en ON dictionary.location (nameEn)');
        $this->addSql('CREATE INDEX idx_location_trash ON dictionary.location (trash)');
        $this->addSql('CREATE TABLE dictionary.region (id SERIAL NOT NULL, nameRu VARCHAR(255) NOT NULL, nameEn VARCHAR(255) NOT NULL, trash BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_region_name_ru ON dictionary.region (nameRu)');
        $this->addSql('CREATE INDEX idx_region_name_en ON dictionary.region (nameEn)');
        $this->addSql('CREATE INDEX idx_region_trash ON dictionary.region (trash)');
        $this->addSql('CREATE TABLE dictionary.resort (id INT NOT NULL, country_id INT DEFAULT NULL, nameRu VARCHAR(255) NOT NULL, nameEn VARCHAR(255) NOT NULL, trash BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_F70FBABEF92F3E70 ON dictionary.resort (country_id)');
        $this->addSql('CREATE INDEX idx_resort_name_ru ON dictionary.resort (nameRu)');
        $this->addSql('CREATE INDEX idx_resort_name_en ON dictionary.resort (nameEn)');
        $this->addSql('CREATE INDEX idx_resort_trash ON dictionary.resort (trash)');
        $this->addSql('CREATE TABLE dictionary.state_region (id INT NOT NULL, country_id INT DEFAULT NULL, name_ru TEXT NOT NULL, name_en TEXT NOT NULL, from_name TEXT DEFAULT NULL, to_name TEXT DEFAULT NULL, in_name TEXT DEFAULT NULL, trash BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_DFEA698CF92F3E70 ON dictionary.state_region (country_id)');
        $this->addSql('CREATE INDEX idx_state_region_name_ru ON dictionary.state_region (name_ru)');
        $this->addSql('CREATE INDEX idx_state_region_name_en ON dictionary.state_region (name_en)');
        $this->addSql('CREATE INDEX idx_state_region_trash ON dictionary.state_region (trash)');
        $this->addSql('CREATE TABLE dictionary.underground (id INT NOT NULL, city_id INT DEFAULT NULL, nameRu VARCHAR(255) NOT NULL, nameEn VARCHAR(255) NOT NULL, trash BOOLEAN NOT NULL, latitude DOUBLE PRECISION DEFAULT NULL, longitude DOUBLE PRECISION DEFAULT NULL, coord_x INT DEFAULT NULL, coord_y INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_458F6FC98BAC62AF ON dictionary.underground (city_id)');
        $this->addSql('CREATE INDEX idx_underground_name_ru ON dictionary.underground (nameRu)');
        $this->addSql('CREATE INDEX idx_underground_name_en ON dictionary.underground (nameEn)');
        $this->addSql('CREATE INDEX idx_underground_trash ON dictionary.underground (trash)');
        $this->addSql('ALTER TABLE xxx_user.office_city ADD CONSTRAINT FK_B034F648FFA0C224 FOREIGN KEY (office_id) REFERENCES xxx_user.office (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE xxx_user.office_city ADD CONSTRAINT FK_B034F6488BAC62AF FOREIGN KEY (city_id) REFERENCES dictionary.city (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dictionary.city ADD CONSTRAINT FK_FCE83007A3ABE5D FOREIGN KEY (resort_id) REFERENCES dictionary.resort (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dictionary.city ADD CONSTRAINT FK_FCE8300D0ABC478 FOREIGN KEY (state_region_id) REFERENCES dictionary.state_region (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dictionary.city ADD CONSTRAINT FK_FCE8300F92F3E70 FOREIGN KEY (country_id) REFERENCES dictionary.country (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dictionary.city ADD CONSTRAINT FK_FCE8300AEF640E5 FOREIGN KEY (regionCity_id) REFERENCES dictionary.city (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dictionary.country ADD CONSTRAINT FK_D2EC4AC998260155 FOREIGN KEY (region_id) REFERENCES dictionary.region (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dictionary.location ADD CONSTRAINT FK_1876A8318BAC62AF FOREIGN KEY (city_id) REFERENCES dictionary.city (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dictionary.resort ADD CONSTRAINT FK_F70FBABEF92F3E70 FOREIGN KEY (country_id) REFERENCES dictionary.country (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dictionary.state_region ADD CONSTRAINT FK_DFEA698CF92F3E70 FOREIGN KEY (country_id) REFERENCES dictionary.country (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dictionary.underground ADD CONSTRAINT FK_458F6FC98BAC62AF FOREIGN KEY (city_id) REFERENCES dictionary.city (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE xxx_user.office_city DROP CONSTRAINT FK_B034F648FFA0C224');
        $this->addSql('ALTER TABLE xxx_user.office_city DROP CONSTRAINT FK_B034F6488BAC62AF');
        $this->addSql('ALTER TABLE dictionary.city DROP CONSTRAINT FK_FCE8300AEF640E5');
        $this->addSql('ALTER TABLE dictionary.location DROP CONSTRAINT FK_1876A8318BAC62AF');
        $this->addSql('ALTER TABLE dictionary.underground DROP CONSTRAINT FK_458F6FC98BAC62AF');
        $this->addSql('ALTER TABLE dictionary.city DROP CONSTRAINT FK_FCE8300F92F3E70');
        $this->addSql('ALTER TABLE dictionary.resort DROP CONSTRAINT FK_F70FBABEF92F3E70');
        $this->addSql('ALTER TABLE dictionary.state_region DROP CONSTRAINT FK_DFEA698CF92F3E70');
        $this->addSql('ALTER TABLE dictionary.country DROP CONSTRAINT FK_D2EC4AC998260155');
        $this->addSql('ALTER TABLE dictionary.city DROP CONSTRAINT FK_FCE83007A3ABE5D');
        $this->addSql('ALTER TABLE dictionary.city DROP CONSTRAINT FK_FCE8300D0ABC478');
        $this->addSql('DROP SEQUENCE dictionary.city_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE dictionary.location_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE dictionary.resort_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE dictionary.state_region_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE dictionary.underground_id_seq CASCADE');
        $this->addSql('DROP TABLE xxx_user.office');
        $this->addSql('DROP TABLE xxx_user.office_city');
        $this->addSql('DROP TABLE dictionary.city');
        $this->addSql('DROP TABLE dictionary.country');
        $this->addSql('DROP TABLE dictionary.currency');
        $this->addSql('DROP TABLE dictionary.location');
        $this->addSql('DROP TABLE dictionary.region');
        $this->addSql('DROP TABLE dictionary.resort');
        $this->addSql('DROP TABLE dictionary.state_region');
        $this->addSql('DROP TABLE dictionary.underground');
    }
}
