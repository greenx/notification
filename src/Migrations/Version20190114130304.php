<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190114130304 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE event.translations DROP CONSTRAINT fk_4cc67575232d562b');
        $this->addSql('ALTER TABLE notification DROP CONSTRAINT fk_bf5476ca71f7e88b');
        $this->addSql('DROP SEQUENCE event.translation_id_seq CASCADE');
        $this->addSql('CREATE TABLE event.event (id SERIAL NOT NULL, event VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, subject VARCHAR(255) NOT NULL, message TEXT NOT NULL, transport VARCHAR(255) CHECK(transport IN (\'MAIL\', \'SMS\', \'PUSH\')) NOT NULL, application VARCHAR(255) CHECK(application IN (\'HOTEL\', \'ORDER\', \'OFFLINE_ORDER\')) NOT NULL, message_type VARCHAR(255) CHECK(message_type IN (\'PLAIN\', \'HTML\')) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_BACB65EF3BAE0AA7 ON event.event (event)');
        $this->addSql('COMMENT ON COLUMN event.event.transport IS \'(DC2Type:TransportInterface)\'');
        $this->addSql('COMMENT ON COLUMN event.event.application IS \'(DC2Type:Application)\'');
        $this->addSql('COMMENT ON COLUMN event.event.message_type IS \'(DC2Type:MessageType)\'');
        $this->addSql('DROP TABLE event.translation');
        $this->addSql('ALTER TABLE event.translations DROP CONSTRAINT IF EXISTS FK_4CC67575232D562B');
        $this->addSql('ALTER TABLE event.translations ADD CONSTRAINT FK_4CC67575232D562B FOREIGN KEY (object_id) REFERENCES event.event (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE notification DROP CONSTRAINT IF EXISTS FK_BF5476CA71F7E88B');
        $this->addSql('ALTER TABLE notification ADD CONSTRAINT FK_BF5476CA71F7E88B FOREIGN KEY (event_id) REFERENCES event.event (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE event.translations DROP CONSTRAINT FK_4CC67575232D562B');
        $this->addSql('ALTER TABLE notification DROP CONSTRAINT FK_BF5476CA71F7E88B');
        $this->addSql('CREATE SEQUENCE event.translation_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE event.translation (id SERIAL NOT NULL, event VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, subject VARCHAR(255) NOT NULL, message TEXT NOT NULL, transport VARCHAR(255) CHECK(transport IN (\'MAIL\', \'SMS\', \'PUSH\')) NOT NULL, application VARCHAR(255) CHECK(application IN (\'HOTEL\', \'ORDER\', \'OFFLINE_ORDER\')) NOT NULL, message_type VARCHAR(255) CHECK(message_type IN (\'PLAIN\', \'HTML\')) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX uniq_a00f5b743bae0aa7 ON event.translation (event)');
        $this->addSql('COMMENT ON COLUMN event.translation.transport IS \'(DC2Type:TransportInterface)\'');
        $this->addSql('COMMENT ON COLUMN event.translation.application IS \'(DC2Type:Application)\'');
        $this->addSql('COMMENT ON COLUMN event.translation.message_type IS \'(DC2Type:MessageType)\'');
        $this->addSql('DROP TABLE event.event');
        $this->addSql('ALTER TABLE event.translations DROP CONSTRAINT IF EXISTS fk_4cc67575232d562b');
        $this->addSql('ALTER TABLE event.translations ADD CONSTRAINT fk_4cc67575232d562b FOREIGN KEY (object_id) REFERENCES event.translation (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE notification DROP CONSTRAINT IF EXISTS fk_bf5476ca71f7e88b');
        $this->addSql('ALTER TABLE notification ADD CONSTRAINT fk_bf5476ca71f7e88b FOREIGN KEY (event_id) REFERENCES event.translation (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
