<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190214094520 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA notification');
        $this->addSql('ALTER TABLE appnotification.notification DROP CONSTRAINT fk_4b91836571f7e88b');
        $this->addSql('ALTER TABLE appnotification.recipient DROP CONSTRAINT fk_993440e271f7e88b');
        $this->addSql('ALTER TABLE event_translation DROP CONSTRAINT fk_1fe096ef2c2ac5d3');
        $this->addSql('ALTER TABLE appnotification.notification_log DROP CONSTRAINT fk_a48bf094ef1a9d84');
        $this->addSql('DROP SEQUENCE appnotification.notification_log_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE appnotification.event_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE appnotification.notification_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE appnotification.recipient_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE notification.notification_log_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE notification.notification (id SERIAL NOT NULL, event_id INT NOT NULL, account_id INT DEFAULT NULL, item_id INT DEFAULT NULL, recipient VARCHAR(100) NOT NULL, subject VARCHAR(255) NOT NULL, message TEXT NOT NULL, transport_status VARCHAR(255) CHECK(transport_status IN (\'NEW\', \'SENT\', \'DELIVERED\', \'READ\', \'ERROR\')) NOT NULL, transport VARCHAR(255) CHECK(transport IN (\'MAIL\', \'SMS\', \'PUSH\')) NOT NULL, message_type VARCHAR(255) CHECK(message_type IN (\'PLAIN\', \'HTML\')) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_D93839FE71F7E88B ON notification.notification (event_id)');
        $this->addSql('CREATE INDEX IDX_D93839FE9B6B5FBA ON notification.notification (account_id)');
        $this->addSql('COMMENT ON COLUMN notification.notification.transport_status IS \'(DC2Type:TransportStatus)\'');
        $this->addSql('COMMENT ON COLUMN notification.notification.transport IS \'(DC2Type:Transport)\'');
        $this->addSql('COMMENT ON COLUMN notification.notification.message_type IS \'(DC2Type:MessageType)\'');
        $this->addSql('CREATE TABLE notification.notification_log (id INT NOT NULL, notification_id INT NOT NULL, account_id INT DEFAULT NULL, transport_status VARCHAR(255) CHECK(transport_status IN (\'NEW\', \'SENT\', \'DELIVERED\', \'READ\', \'ERROR\')) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_67C7183FEF1A9D84 ON notification.notification_log (notification_id)');
        $this->addSql('CREATE INDEX IDX_67C7183F9B6B5FBA ON notification.notification_log (account_id)');
        $this->addSql('COMMENT ON COLUMN notification.notification_log.transport_status IS \'(DC2Type:TransportStatus)\'');
        $this->addSql('CREATE TABLE notification.event (id SERIAL NOT NULL, event VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, transport VARCHAR(255) CHECK(transport IN (\'MAIL\', \'SMS\', \'PUSH\')) NOT NULL, application VARCHAR(255) CHECK(application IN (\'HOTEL\', \'ORDER\', \'OFFLINE_ORDER\')) NOT NULL, message_type VARCHAR(255) CHECK(message_type IN (\'PLAIN\', \'HTML\')) NOT NULL, json_schema_url VARCHAR(255) DEFAULT NULL, item_id_template VARCHAR(255) DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN notification.event.transport IS \'(DC2Type:Transport)\'');
        $this->addSql('COMMENT ON COLUMN notification.event.application IS \'(DC2Type:Application)\'');
        $this->addSql('COMMENT ON COLUMN notification.event.message_type IS \'(DC2Type:MessageType)\'');
        $this->addSql('CREATE TABLE notification.event_translation (id INT NOT NULL, translatable_id INT DEFAULT NULL, subject VARCHAR(255) NOT NULL, message TEXT NOT NULL, locale VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_FA5C3EA72C2AC5D3 ON notification.event_translation (translatable_id)');
        $this->addSql('CREATE UNIQUE INDEX event_translation_unique_translation ON notification.event_translation (translatable_id, locale)');
        $this->addSql('CREATE TABLE notification.recipient (id SERIAL NOT NULL, event_id INT NOT NULL, argument VARCHAR(255) NOT NULL, recipient_type VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_D76652B071F7E88B ON notification.recipient (event_id)');
        $this->addSql('ALTER TABLE notification.notification ADD CONSTRAINT FK_D93839FE71F7E88B FOREIGN KEY (event_id) REFERENCES notification.event (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE notification.notification ADD CONSTRAINT FK_D93839FE9B6B5FBA FOREIGN KEY (account_id) REFERENCES xxx_security.account (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE notification.notification_log ADD CONSTRAINT FK_67C7183FEF1A9D84 FOREIGN KEY (notification_id) REFERENCES notification.notification (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE notification.notification_log ADD CONSTRAINT FK_67C7183F9B6B5FBA FOREIGN KEY (account_id) REFERENCES xxx_security.account (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE notification.event_translation ADD CONSTRAINT FK_FA5C3EA72C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES notification.event (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE notification.recipient ADD CONSTRAINT FK_D76652B071F7E88B FOREIGN KEY (event_id) REFERENCES notification.event (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE appnotification.event');
        $this->addSql('DROP TABLE appnotification.notification');
        $this->addSql('DROP TABLE appnotification.notification_log');
        $this->addSql('DROP TABLE appnotification.recipient');
        $this->addSql('DROP TABLE event_translation');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA event');
        $this->addSql('CREATE SCHEMA appnotification');
        $this->addSql('ALTER TABLE notification.notification_log DROP CONSTRAINT FK_67C7183FEF1A9D84');
        $this->addSql('ALTER TABLE notification.notification DROP CONSTRAINT FK_D93839FE71F7E88B');
        $this->addSql('ALTER TABLE notification.event_translation DROP CONSTRAINT FK_FA5C3EA72C2AC5D3');
        $this->addSql('ALTER TABLE notification.recipient DROP CONSTRAINT FK_D76652B071F7E88B');
        $this->addSql('DROP SEQUENCE notification.notification_log_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE appnotification.notification_log_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE appnotification.event_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE appnotification.notification_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE appnotification.recipient_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE appnotification.event (id SERIAL NOT NULL, event VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, transport VARCHAR(255) CHECK(transport IN (\'MAIL\', \'SMS\', \'PUSH\')) NOT NULL, application VARCHAR(255) CHECK(application IN (\'HOTEL\', \'ORDER\', \'OFFLINE_ORDER\')) NOT NULL, message_type VARCHAR(255) CHECK(message_type IN (\'PLAIN\', \'HTML\')) NOT NULL, json_schema_url VARCHAR(255) DEFAULT NULL, item_id_template VARCHAR(255) DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN appnotification.event.transport IS \'(DC2Type:Transport)\'');
        $this->addSql('COMMENT ON COLUMN appnotification.event.application IS \'(DC2Type:Application)\'');
        $this->addSql('COMMENT ON COLUMN appnotification.event.message_type IS \'(DC2Type:MessageType)\'');
        $this->addSql('CREATE TABLE appnotification.notification (id SERIAL NOT NULL, event_id INT NOT NULL, account_id INT DEFAULT NULL, item_id INT DEFAULT NULL, recipient VARCHAR(100) NOT NULL, subject VARCHAR(255) NOT NULL, message TEXT NOT NULL, transport_status VARCHAR(255) CHECK(transport_status IN (\'NEW\', \'SENT\', \'DELIVERED\', \'READ\', \'ERROR\')) NOT NULL, transport VARCHAR(255) CHECK(transport IN (\'MAIL\', \'SMS\', \'PUSH\')) NOT NULL, message_type VARCHAR(255) CHECK(message_type IN (\'PLAIN\', \'HTML\')) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_4b91836571f7e88b ON appnotification.notification (event_id)');
        $this->addSql('CREATE INDEX idx_4b9183659b6b5fba ON appnotification.notification (account_id)');
        $this->addSql('COMMENT ON COLUMN appnotification.notification.transport_status IS \'(DC2Type:TransportStatus)\'');
        $this->addSql('COMMENT ON COLUMN appnotification.notification.transport IS \'(DC2Type:Transport)\'');
        $this->addSql('COMMENT ON COLUMN appnotification.notification.message_type IS \'(DC2Type:MessageType)\'');
        $this->addSql('CREATE TABLE appnotification.notification_log (id INT NOT NULL, notification_id INT NOT NULL, account_id INT DEFAULT NULL, transport_status VARCHAR(255) CHECK(transport_status IN (\'NEW\', \'SENT\', \'DELIVERED\', \'READ\', \'ERROR\')) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_a48bf0949b6b5fba ON appnotification.notification_log (account_id)');
        $this->addSql('CREATE INDEX idx_a48bf094ef1a9d84 ON appnotification.notification_log (notification_id)');
        $this->addSql('COMMENT ON COLUMN appnotification.notification_log.transport_status IS \'(DC2Type:TransportStatus)\'');
        $this->addSql('CREATE TABLE appnotification.recipient (id SERIAL NOT NULL, event_id INT NOT NULL, argument VARCHAR(255) NOT NULL, recipient_type VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_993440e271f7e88b ON appnotification.recipient (event_id)');
        $this->addSql('CREATE TABLE event_translation (id INT NOT NULL, translatable_id INT DEFAULT NULL, subject VARCHAR(255) NOT NULL, message TEXT NOT NULL, locale VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX event_translation_unique_translation ON event_translation (translatable_id, locale)');
        $this->addSql('CREATE INDEX idx_1fe096ef2c2ac5d3 ON event_translation (translatable_id)');
        $this->addSql('ALTER TABLE appnotification.notification ADD CONSTRAINT fk_4b91836571f7e88b FOREIGN KEY (event_id) REFERENCES appnotification.event (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE appnotification.notification ADD CONSTRAINT fk_4b9183659b6b5fba FOREIGN KEY (account_id) REFERENCES xxx_security.account (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE appnotification.notification_log ADD CONSTRAINT fk_a48bf094ef1a9d84 FOREIGN KEY (notification_id) REFERENCES appnotification.notification (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE appnotification.notification_log ADD CONSTRAINT fk_a48bf0949b6b5fba FOREIGN KEY (account_id) REFERENCES xxx_security.account (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE appnotification.recipient ADD CONSTRAINT fk_993440e271f7e88b FOREIGN KEY (event_id) REFERENCES appnotification.event (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE event_translation ADD CONSTRAINT fk_1fe096ef2c2ac5d3 FOREIGN KEY (translatable_id) REFERENCES appnotification.event (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE notification.notification');
        $this->addSql('DROP TABLE notification.notification_log');
        $this->addSql('DROP TABLE notification.event');
        $this->addSql('DROP TABLE notification.event_translation');
        $this->addSql('DROP TABLE notification.recipient');
    }
}
