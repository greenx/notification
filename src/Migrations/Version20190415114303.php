<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190415114303 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE notification.event_xxx_user (event_id INT NOT NULL, xxx_user_id INT NOT NULL, PRIMARY KEY(event_id, xxx_user_id))');
        $this->addSql('CREATE INDEX IDX_8FCD5BE171F7E88B ON notification.event_xxx_user (event_id)');
        $this->addSql('CREATE INDEX IDX_8FCD5BE1773FE612 ON notification.event_xxx_user (xxx_user_id)');
        $this->addSql('CREATE TABLE notification.event_rule (id SERIAL NOT NULL, event_id INT NOT NULL, argument VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_9DCDC87671F7E88B ON notification.event_rule (event_id)');
        $this->addSql('ALTER TABLE notification.event_xxx_user ADD CONSTRAINT FK_8FCD5BE171F7E88B FOREIGN KEY (event_id) REFERENCES notification.event (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE notification.event_xxx_user ADD CONSTRAINT FK_8FCD5BE1773FE612 FOREIGN KEY (xxx_user_id) REFERENCES xxx_user.account (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE notification.event_rule ADD CONSTRAINT FK_9DCDC87671F7E88B FOREIGN KEY (event_id) REFERENCES notification.event (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE notification.notification ADD CONSTRAINT FK_D93839FE9B6B5FBA FOREIGN KEY (account_id) REFERENCES xxx_user.account (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE notification.notification_log ADD CONSTRAINT FK_67C7183F9B6B5FBA FOREIGN KEY (account_id) REFERENCES xxx_user.account (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE notification.event ADD is_internal BOOLEAN NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP TABLE notification.event_xxx_user');
        $this->addSql('DROP TABLE notification.event_rule');
        $this->addSql('ALTER TABLE notification.notification DROP CONSTRAINT FK_D93839FE9B6B5FBA');
        $this->addSql('ALTER TABLE notification.notification_log DROP CONSTRAINT FK_67C7183F9B6B5FBA');
        $this->addSql('ALTER TABLE notification.event DROP is_internal');
    }
}
