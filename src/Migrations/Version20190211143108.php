<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190211143108 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE appnotification.event_translations_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE event_translation_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE event_translation (id INT NOT NULL, translatable_id INT DEFAULT NULL, subject VARCHAR(255) NOT NULL, message TEXT NOT NULL, locale VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_1FE096EF2C2AC5D3 ON event_translation (translatable_id)');
        $this->addSql('CREATE UNIQUE INDEX event_translation_unique_translation ON event_translation (translatable_id, locale)');
        $this->addSql('ALTER TABLE event_translation ADD CONSTRAINT FK_1FE096EF2C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES appnotification.event (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE appnotification.event_translations');
        $this->addSql('ALTER TABLE appnotification.notification ALTER created_at DROP NOT NULL');
        $this->addSql('ALTER TABLE appnotification.notification ALTER updated_at DROP NOT NULL');
        $this->addSql('ALTER TABLE appnotification.notification_log ALTER created_at DROP NOT NULL');
        $this->addSql('ALTER TABLE appnotification.notification_log ALTER updated_at DROP NOT NULL');
        $this->addSql('ALTER TABLE appnotification.event DROP subject');
        $this->addSql('ALTER TABLE appnotification.event DROP message');
        $this->addSql('ALTER TABLE appnotification.event ALTER created_at DROP NOT NULL');
        $this->addSql('ALTER TABLE appnotification.event ALTER updated_at DROP NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA event');
        $this->addSql('DROP SEQUENCE event_translation_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE appnotification.event_translations_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE appnotification.event_translations (id SERIAL NOT NULL, object_id INT DEFAULT NULL, locale VARCHAR(8) NOT NULL, field VARCHAR(32) NOT NULL, content TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_b064695a232d562b ON appnotification.event_translations (object_id)');
        $this->addSql('CREATE UNIQUE INDEX event_translation_unique_idx ON appnotification.event_translations (locale, object_id, field)');
        $this->addSql('ALTER TABLE appnotification.event_translations ADD CONSTRAINT fk_b064695a232d562b FOREIGN KEY (object_id) REFERENCES appnotification.event (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE event_translation');
        $this->addSql('ALTER TABLE appnotification.notification ALTER created_at SET NOT NULL');
        $this->addSql('ALTER TABLE appnotification.notification ALTER updated_at SET NOT NULL');
        $this->addSql('ALTER TABLE appnotification.notification_log ALTER created_at SET NOT NULL');
        $this->addSql('ALTER TABLE appnotification.notification_log ALTER updated_at SET NOT NULL');
        $this->addSql('ALTER TABLE appnotification.event ADD subject VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE appnotification.event ADD message TEXT NOT NULL');
        $this->addSql('ALTER TABLE appnotification.event ALTER created_at SET NOT NULL');
        $this->addSql('ALTER TABLE appnotification.event ALTER updated_at SET NOT NULL');
    }
}
