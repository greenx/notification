<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190427111853 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE xxx_user.register_data (id SERIAL NOT NULL, city_id INT NOT NULL, contract_type VARCHAR(255) CHECK(contract_type IN (\'AGENCY_CONTRACT_TYPE\', \'SERVICE_CONTRACT_TYPE\')) NOT NULL, legal_name VARCHAR(255) NOT NULL, tax_system VARCHAR(255) NOT NULL, legal_address VARCHAR(512) NOT NULL, real_address VARCHAR(512) NOT NULL, site VARCHAR(255) DEFAULT NULL, email VARCHAR(255) NOT NULL, phone VARCHAR(255) NOT NULL, activity VARCHAR(255) DEFAULT NULL, additional_info TEXT DEFAULT NULL, source_info VARCHAR(512) DEFAULT NULL, promo VARCHAR(255) DEFAULT NULL, chief_fio VARCHAR(255) NOT NULL, position VARCHAR(255) NOT NULL, founding_document VARCHAR(255) NOT NULL, general_acc_fio VARCHAR(255) NOT NULL, inn VARCHAR(24) NOT NULL, kpp VARCHAR(64) NOT NULL, okpo VARCHAR(64) NOT NULL, okfs VARCHAR(64) NOT NULL, bank_rs VARCHAR(64) NOT NULL, bank_name VARCHAR(255) NOT NULL, bank_cs VARCHAR(64) NOT NULL, bank_bic VARCHAR(64) NOT NULL, contact_person VARCHAR(512) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_AD7FA6D08BAC62AF ON xxx_user.register_data (city_id)');
        $this->addSql('COMMENT ON COLUMN xxx_user.register_data.contract_type IS \'(DC2Type:ContractType)\'');
        $this->addSql('ALTER TABLE xxx_user.register_data ADD CONSTRAINT FK_AD7FA6D08BAC62AF FOREIGN KEY (city_id) REFERENCES dictionary.city (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE xxx_user.legal ADD register_data_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE xxx_user.legal ADD CONSTRAINT FK_EF4280E477991A22 FOREIGN KEY (register_data_id) REFERENCES xxx_user.register_data (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_EF4280E477991A22 ON xxx_user.legal (register_data_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE xxx_user.legal DROP CONSTRAINT FK_EF4280E477991A22');
        $this->addSql('DROP TABLE xxx_user.register_data');
        $this->addSql('DROP INDEX UNIQ_EF4280E477991A22');
        $this->addSql('ALTER TABLE xxx_user.legal DROP register_data_id');

    }
}
