<?php

namespace App\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

/**
 * Class TransportType
 * @package App\DBAL\Types
 *
 * Enum type for "eventType".
 */

final class TransportType extends AbstractEnumType
{
    public const MAIL = 'MAIL';
    public const SMS = 'SMS';
    public const PUSH = 'PUSH';


    protected static $choices = [
        self::MAIL => 'Электроная почта',
        self::SMS => 'СМС',
        self::PUSH => 'Пуш уведомление',
    ];
}