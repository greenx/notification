<?php

namespace App\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

/**
 * Class TransportStatusType
 * @package App\DBAL\Types
 *
 * Enum type for "eventType".
 */

final class TransportStatusType extends AbstractEnumType
{
    public const NEW = 'NEW';
    public const SENT = 'SENT';
    public const DELIVERED = 'DELIVERED';
    public const READ = 'READ';
    public const ERROR = 'ERROR';


    protected static $choices = [
        self::NEW => 'Новое',
        self::SENT => 'Отправлено',
        self::DELIVERED => 'Доставлено',
        self::READ => 'Прочитано',
        self::ERROR => 'Ошибка',
    ];
}