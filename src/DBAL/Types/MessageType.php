<?php

namespace App\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

/**
 * Class MessageType
 * @package App\DBAL\Types
 *
 * Enum type for "messageType".
 */

final class MessageType extends AbstractEnumType
{
    public const PLAIN = 'PLAIN';
    public const HTML = 'HTML';


    protected static $choices = [
        self::PLAIN => 'Текст',
        self::HTML => 'HTML',
    ];
}