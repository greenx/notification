<?php
/**
 * Created by PhpStorm.
 * User: greenx
 * Date: 04/05/2019
 * Time: 19:21
 */

namespace App\DTO;


class Attachment
{
    /**
     * @var string
     */
    private $path;

    /**
     * @var string
     */
    private $name;

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param string $path
     * @return Attachment
     */
    public function setPath(string $path): Attachment
    {
        $this->path = $path;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Attachment
     */
    public function setName(string $name): Attachment
    {
        $this->name = $name;
        return $this;
    }
}