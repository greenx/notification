<?php

namespace App\Service\Message\Attachment;

use Doctrine\Common\Collections\ArrayCollection;


interface AttachmentInterface
{
    public function get(object $message): ArrayCollection;
}