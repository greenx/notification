<?php

namespace App\Service\Message\Attachment;

use Doctrine\Common\Collections\ArrayCollection;

class Attachment implements AttachmentInterface
{
    /**
     * @param object $message
     * @return ArrayCollection
     */
    public function get(object $message): ArrayCollection
    {
        $attachments = new ArrayCollection();

        if (isset($message->attachments)) {
            foreach ($message->attachments as $attachment) {
                if ($attachment->path) {
                    $obj = (new \App\DTO\Attachment())
                        ->setPath($attachment->path ?? null)
                        ->setName($attachment->name ?? null);
                    $attachments->add($obj);
                }
            }
        }

        return $attachments;
    }
}