<?php

namespace App\Service\Message;

use xxx\NotificationAdminBundle\Entity\Event;
use xxx\NotificationAdminBundle\Entity\EventRule;
use Psr\Log\LoggerInterface;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Symfony\Component\Serializer\SerializerInterface;


class Rules
{
    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var ExpressionLanguage
     */
    private $expressionLanguage;
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param SerializerInterface $serializer
     * @param LoggerInterface $logger
     */
    public function __construct(SerializerInterface $serializer, LoggerInterface $logger)
    {
        $this->serializer = $serializer;
        $this->expressionLanguage = new ExpressionLanguage();
        $this->logger = $logger;
    }

    /**
     * @param Event $event
     * @param object $message
     * @return bool
     */
    public function isSendable(Event $event, object $message): bool
    {
        // Нет правил, отправялем
        if(!$event->getRules()->count())
        {
            return true;
        }

        /** @var EventRule $rule */
        foreach ($event->getRules() as $rule) {
            try {
                $result = $this->expressionLanguage->evaluate($rule->getArgument(), ['msg' => $message]);
                if ($result) {
                    return true;
                }
            } catch (\Exception $exception) {
                $this->logger->error(\sprintf("Rule error: ID %s, %s.", $rule->getId(), $exception->getMessage()));
            }
        }

        return false;
    }
}