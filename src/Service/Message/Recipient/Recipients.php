<?php

namespace App\Service\Message\Recipient;

use App\Service\Message\Recipient\RecipientInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use xxx\NotificationAdminBundle\Entity\Event;
use xxx\NotificationAdminBundle\Entity\Recipient;
use xxx\NotificationAdminBundle\Entity\RecipientDirect;
use xxx\NotificationAdminBundle\Entity\RecipientInside;
use xxx\NotificationAdminBundle\Entity\RecipientRemote;
use GuzzleHttp\Client;
use xxx\NotificationAdminBundle\Entity\RecipientStatic;
use xxx\UserAdminBundle\Entity\xxxUser;
use App\DTO\RecipientAccount;
use Psr\Log\LoggerInterface;


class Recipients implements RecipientInterface
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(Client $client, EntityManagerInterface $entityManager, LoggerInterface $logger)
    {
        $this->client = $client;
        $this->entityManager = $entityManager;
        $this->logger = $logger;
    }

    /**
     * @param Event $event
     * @param object $message
     * @return ArrayCollection|null
     */
    public function get(Event $event, object $message): ?ArrayCollection
    {
        $this->logger->info('Get recipients accounts');

        return $this->getRecipients($event->getRecipients()->getValues(), $message);
    }

    /**
     * @param array $recipients
     * @param object $message
     * @return ArrayCollection
     */
    private function getRecipients(array $recipients, object $message): ArrayCollection
    {
        $recipientAccounts = new ArrayCollection();

        /** @var Recipient $recipient */
        foreach ($recipients as $recipient) {
            $account = false;
            $recipientAccount = new RecipientAccount();

            switch (true) {

                // Email пользователя из сторонних источников
                case $recipient instanceof RecipientRemote:
                    $account = $this->getRemoteRecipient($recipient, $message);
                    break;
                // Email пользователя из БД
                case $recipient instanceof RecipientStatic:
                    $account = $this->getAccountByRecipientId((int)$recipient->getArgument());
                    break;
                // ID пользователя из данных Event по шаблону, Email получаем из БД
                case $recipient instanceof RecipientInside:
                    $recipientId = $recipient->getRecipientId((array)$message);

                    if ($recipientId) {
                        $account = $this->getAccountByRecipientId($recipientId);
                    } else {
                        $this->logger->warning(\sprintf('Inside recipient pattern %s, message %s not found ',
                            $recipient->getArgument(), json_encode((array)$message)));
                    }
                    break;
                // Email пользователя из данных Event по шаблону
                case $recipient instanceof RecipientDirect:
                    $account = filter_var($recipient->getRecipientEmail((array)$message), FILTER_VALIDATE_EMAIL);
                    break;
            }

            if ($account && !$recipientAccounts->contains($account)) {
                if ($account instanceof xxxUser) {
                    /** @var xxxUser $account */
                    foreach ($account->getEmails() as $email) {
                        $recipientAccount->setEmail($email);
                    }
                } else {
                    $recipientAccount->setEmail($account);
                }
                $recipientAccounts->add($recipientAccount);
            }
        }

        if (!$recipientAccounts->count()) {
            $this->logger->warning('Recipients account not found');
        }

        return $recipientAccounts;
    }

    /**
     * @param RecipientRemote $recipient
     * @param object $message
     * @return null|string
     */
    private function getRemoteRecipient(RecipientRemote $recipient, object $message): ?string
    {
        $url = $recipient->getRecipientUrl((array)$message);
        if (!empty($url)) {
            try {
                $response = $this->client->get($url);
                $content = $response->getBody()->getContents();
                if (filter_var($content, FILTER_VALIDATE_IP)) {
                    return $content;
                }
            } catch (\Exception $e) {
                $this->logger->error(\sprintf('Get remote %s recipient error %s', $url, $e->getMessage()));
                return null;
            }
        }

        $this->logger->warning(\sprintf('Remote recipient %s not found ', $url));
        return null;
    }

    /**
     * @param int $recipientId
     * @return xxxUser|null
     */
    private function getAccountByRecipientId(int $recipientId): ?xxxUser
    {
        try {
            $user = $this->entityManager->getRepository(xxxUser::class)->find($recipientId);
        } catch (\Exception $e) {
            $this->logger->error(\sprintf('Static recipient %s error: $s', $recipientId, $e->getMessage()));
            return null;
        }

        if (!$user) {
            $this->logger->warning(\sprintf('Recipient with ID %s not found ', $recipientId));
        }

        return $user ?? null;
    }
}