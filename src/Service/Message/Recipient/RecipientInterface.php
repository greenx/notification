<?php
/**
 * Created by PhpStorm.
 * User: greenx
 * Date: 04/05/2019
 * Time: 19:35
 */

namespace App\Service\Message\Recipient;

use Doctrine\Common\Collections\ArrayCollection;
use xxx\NotificationAdminBundle\Entity\Event;


interface RecipientInterface
{
    public function get(Event $event, object $message): ?ArrayCollection;
}