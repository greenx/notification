<?php

namespace App\Service\Message;

use App\DBAL\Types\TransportType;
use App\DTO\Attachment;
use App\DTO\RecipientAccount;
use App\Entity\NotificationAttachment;
use App\Service\Message\Attachment\AttachmentInterface;
use App\Service\Message\Compose\ComposeInterface;
use App\Service\Message\Recipient\RecipientInterface;
use xxx\NotificationAdminBundle\Entity\Event;
use App\Entity\Notification;
use App\Event\NotificationCreatedEvent;
use App\Exception\CreateNotificationException;
use App\Service\Transport\Contract\TransportInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class MessageService
{
    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;
    /**
     * @var TransportInterface
     */
    private $transport;
    /**
     * @var ComposeInterface
     */
    private $composeMessage;
    /**
     * @var Recipients
     */
    private $recipients;
    /**
     * @var rules
     */
    private $rules;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var AttachmentInterface
     */
    private $attachment;

    /**
     * MessageService constructor.
     * @param TransportInterface $transport
     * @param ComposeInterface $composeMessage
     * @param RecipientsInterface $recipients
     * @param Rules $rules
     * @param EntityManagerInterface $em
     * @param EventDispatcherInterface $dispatcher
     * @param LoggerInterface $logger
     * @param AttachmentInterface $attachment
     */
    public function __construct(
        TransportInterface $transport,
        ComposeInterface $composeMessage,
        RecipientInterface $recipients,
        EntityManagerInterface $em,
        EventDispatcherInterface $dispatcher,
        LoggerInterface $logger,
        AttachmentInterface $attachment,
        Rules $rules
    ) {

        $this->em = $em;
        $this->dispatcher = $dispatcher;
        $this->transport = $transport;
        $this->composeMessage = $composeMessage;
        $this->recipients = $recipients;
        $this->rules = $rules;
        $this->logger = $logger;
        $this->attachment = $attachment;
    }

    /**
     * @param Event $event
     * @param object $message
     * @throws CreateNotificationException
     * @throws \Throwable
     */
    public function send(Event $event, object $message)
    {
        $this->logger->info('Process message filters');

        if ($this->rules->isSendable($event, $message)) {

            $this->logger->info('Compose and send message');

            $recipients = $this->recipients->get($event, $message);
            $messageSubject = $this->composeMessage->compose($event->getSubject(), (array)$message);
            $messageBody = $this->composeMessage->compose($event->getMessage(), (array)$message);
            $attachments = $this->attachment->get($message);
            $itemId = $this->getItemId($event->getItemIdTemplate(), (array)$message);
            $userId = null;

            /** @var RecipientAccount $recipient */
            foreach ($recipients as $recipient) {
                try {
                    $notification = new Notification();
                    $notification->setTransport(TransportType::MAIL);
                    $notification->setEvent($event);
                    $notification->setRecipient($recipient->getEmail());
                    $notification->setSubject($messageSubject);
                    $notification->setMessage($messageBody);
                    $notification->setItemId($itemId);
                    $notification->setAccount($userId);
                    $notification->setMessageType($event->getMessageType());

                    /** @var Attachment $attachment */
                    foreach ($attachments as $attachment) {
                        $newAttachment = new NotificationAttachment();
                        $newAttachment
                            ->setName($attachment->getName())
                            ->setPath($attachment->getPath());
                        $notification->addNotificationAttachment($newAttachment);
                    }

                    $this->em->persist($notification);
                    $this->em->flush();

                    $this->logger->info(\sprintf('Notification created: %s to %s', $messageSubject,
                        $recipient->getEmail()));
                } catch (\Exception $exception) {
                    throw new CreateNotificationException($exception->getMessage());
                }

                $event_create = new NotificationCreatedEvent($notification);
                $this->dispatcher->dispatch('notification.created', $event_create);
            }
        }
    }

    /**
     * @param null|string $path
     * @param array $eventData
     * @return mixed|null
     */
    protected function getItemId(?string $path, array $eventData)
    {
        $path = explode('.', $path);
        return $this->getPropertyValue($path, $eventData);
    }

    /**
     * @param array $path
     * @param array $data
     * @return mixed|null
     */
    protected function getPropertyValue(array $path, array $data)
    {
        $node = array_shift($path);

        if (false !== $node) {
            if (!isset($data[$node])) {
                return null;
            }
            if (count($path)) {
                return $this->getPropertyValue($path, $data[$node]);
            }
            return $data[$node];
        }
        return null;
    }
}