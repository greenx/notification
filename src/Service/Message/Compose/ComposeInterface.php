<?php


namespace App\Service\Message\Compose;


interface ComposeInterface
{
    public function compose(string $template, array $eventData): string;
}