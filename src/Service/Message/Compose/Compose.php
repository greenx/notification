<?php

namespace App\Service\Message\Compose;

use App\Service\Message\Compose\ComposeInterface;
use Twig_Error;

class Compose implements ComposeInterface
{
    private $templating;

    /**
     * Compose constructor.
     * @param \Twig_Environment $templating
     */
    public function __construct(\Twig_Environment $templating)
    {
        $this->templating = $templating;
        $this->templating->disableStrictVariables();
    }

    /**
     * Обработка шаблона сообщения
     * @param string $template
     * @param array $eventData
     * @return string
     * @throws \Throwable
     */
    public function compose(string $template, array $eventData): string
    {
        try {
            return $this->templating->createTemplate($template)->render($eventData);
        } catch (Twig_Error $exception) {
            return $template;
        }
    }
}