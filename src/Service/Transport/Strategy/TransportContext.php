<?php

namespace App\Service\Transport\Strategy;

use App\Entity\Notification;
use App\Service\Transport\Contract\TransportInterface;
use App\Service\Transport\Contract\TransportStrategyInterface;


class TransportContext
{
    private $strategies;

    /**
     * @param TransportStrategyInterface $strategy
     */
    public function addStrategy(TransportStrategyInterface $strategy)
    {
        $this->strategies[] = $strategy;
    }

    /**
     * @param Notification $notification
     * @return TransportInterface
     * @throws \Exception
     */
    public function handle(Notification $notification)
    {
        $transport = $notification->getTransport();

        foreach ($this->strategies as $strategy) {
            /** @var TransportStrategyInterface $strategy */
            if ($strategy->supports($transport)) {
                return $strategy->getService();
            }
        }

        throw new \Exception('No transport scheme strategy was handled, check your conditions');
    }
}