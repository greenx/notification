<?php

namespace App\Service\Transport\Strategy;


use App\DBAL\Types\TransportType;
use App\Service\Transport\Contract\TransportStrategyInterface;
use App\Service\Transport\EmailTransport;
use App\Service\Transport\Contract\TransportInterface;

class EmailTransportStrategy implements TransportStrategyInterface
{
    /**
     * @var EmailTransport
     */
    private $transport;

    public function __construct(EmailTransport $transport)
    {
        $this->transport = $transport;
    }

    /**
     * @param string $transportType
     *
     * @return bool
     */
    public function supports(string $transportType): bool
    {
        return TransportType::MAIL === $transportType;
    }

    /**
     * @return TransportInterface
     */
    public function getService(): TransportInterface
    {
        return $this->transport;
    }
}