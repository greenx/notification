<?php


namespace App\Service\Transport\Contract;

use App\Service\Transport\Contract\TransportInterface;

interface TransportStrategyInterface
{
    /**
     * @param string $payScheme
     * @return bool
     */
    public function supports(string $payScheme): bool;

    /**
     * @return TransportInterface
     */
    public function getService(): TransportInterface;
}