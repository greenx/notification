<?php


namespace App\Service\Transport\Contract;


use App\Entity\Notification;

interface TransportInterface
{
    /**
     * @param Notification $notification
     * @return mixed
     */
    public function send(Notification $notification);
}