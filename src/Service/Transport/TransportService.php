<?php

namespace App\Service\Transport;


use App\Entity\Notification;
use App\Service\Transport\Strategy\TransportContext;

class TransportService
{
    /**
     * @var TransportContext
     */
    private $transportContext;


    public function __construct(TransportContext $transportContext)
    {
        $this->transportContext = $transportContext;

    }

    public function send(Notification $notification)
    {
        $transport = $this->transportContext->handle($notification);
        $transport->send($notification);
    }
}