<?php

namespace App\Service\Transport;

use App\DBAL\Types\TransportStatusType;
use App\DTO\Attachment;
use App\Entity\Notification;
use App\Service\Transport\Contract\TransportInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Swift_Mailer;
use Swift_Message;
use Swift_Attachment;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class EmailTransport implements TransportInterface
{
    private $mailer;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var ParameterBagInterface
     */
    private $params;

    /**
     * EmailTransport constructor.
     * @param array $params
     * @param Swift_Mailer $emailService
     * @param EntityManagerInterface $entityManager
     * @param LoggerInterface $logger
     */
    public function __construct(
        array $params,
        Swift_Mailer $emailService,
        EntityManagerInterface $entityManager,
        LoggerInterface $logger
    ) {
        $this->mailer = $emailService;
        $this->entityManager = $entityManager;
        $this->logger = $logger;
        $this->params = $params;
    }

    /**
     * @param Notification $notification
     * @throws \Exception
     */
    public function send(Notification $notification)
    {
        $this->logger->info('Swift compose message');

        $message = (new Swift_Message())
            ->setFrom($this->params['mailerFrom'])
            ->setSubject($notification->getSubject())
            ->setTo($notification->getRecipient())
            ->setBody(
                $notification->getMessage(),
                'text/' . strtolower($notification->getMessageType())
            );

        /** @var Attachment $attachment */
        foreach ($notification->getAttachments() as $attachment) {
            $this->logger->info('Swift add attachment:' . $attachment->getPath());

            try {
                $attach = Swift_Attachment::fromPath($attachment->getPath());
                if ($attachment->getName()) {
                    $attach->setFilename($attachment->getPath());
                }

                $message->attach($attach);

            } catch (\Exception $exception) {
                $this->logger->warning(
                    \sprintf('Swift add attachment %s error: %s ', $attachment->getPath(), $exception->getMessage())
                );
            }
        }

        try {
            $this->mailer->send($message);
            $this->logger->info('Swift message sent');
        } catch (\Exception $e) {
            $notification->setTransportStatus(TransportStatusType::ERROR);
            $this->entityManager->persist($notification);
            $this->entityManager->flush();

            $this->logger->info('Transport set status: ' . TransportStatusType::ERROR);
            throw new \Exception('Transport Error: ' . $e->getMessage());
        }

        $notification->setTransportStatus(TransportStatusType::SENT);
        $this->entityManager->persist($notification);
        $this->entityManager->flush();
        $this->logger->info('Transport set status: ' . TransportStatusType::SENT);
    }
}