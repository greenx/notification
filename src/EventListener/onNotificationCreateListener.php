<?php

namespace App\EventListener;

use App\Event\NotificationCreatedEvent;
use App\Service\Transport\TransportService;


class onNotificationCreateListener
{
    /**
     * @var TransportService
     */
    private $transportService;

    public function __construct(TransportService $transportService)
    {

        $this->transportService = $transportService;
    }
    /**
     * @param NotificationCreatedEvent $event
     */
    public function notificationCreated(NotificationCreatedEvent $event)
    {
        $this->transportService->send($event->getNotification());
    }
}