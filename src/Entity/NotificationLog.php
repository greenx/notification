<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineEnumAssert;
use xxx\UserAdminBundle\Entity\xxxUser;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Table(name="notification_log", schema="notification")
 * @ORM\Entity(repositoryClass="App\Repository\NotificationLogRepository")
 */
class NotificationLog
{
    use ORMBehaviors\Timestampable\Timestampable;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Notification", inversedBy="notificationLogs")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $notification;

    /**
     * @ORM\Column(type="TransportStatus", length=20, nullable=false)
     * @DoctrineEnumAssert\Enum(entity="App\DBAL\Types\TransportStatusType")
     */
    private $transportStatus;

    /**
     * @ORM\ManyToOne(targetEntity="xxx\UserAdminBundle\Entity\xxxUser")
     */
    private $account;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNotification(): ?Notification
    {
        return $this->notification;
    }

    public function setNotification(?Notification $notification): self
    {
        $this->notification = $notification;

        return $this;
    }

    public function getTransportStatus()
    {
        return $this->transportStatus;
    }

    public function setTransportStatus($transportStatus): self
    {
        $this->transportStatus = $transportStatus;

        return $this;
    }

    public function getAccount(): ?xxxUser
    {
        return $this->account;
    }

    public function setAccount(?xxxUser $account): self
    {
        $this->account = $account;

        return $this;
    }
}
