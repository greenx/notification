<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use xxx\UserAdminBundle\Entity\xxxUser;
use xxx\NotificationAdminBundle\Entity\Event;
use Symfony\Component\Validator\Constraints as Assert;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineEnumAssert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Table(name="notification", schema="notification")
 * @ORM\Entity()
 */
class Notification
{
    use ORMBehaviors\Timestampable\Timestampable;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $itemId;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $recipient;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $subject;

    /**
     * @ORM\Column(type="text")
     */
    private $message;

    /**
     * @ORM\ManyToOne(targetEntity="xxx\NotificationAdminBundle\Entity\Event")
     * @ORM\JoinColumn(nullable=false)
     */
    private $event;

    /**
     * @ORM\ManyToOne(targetEntity="xxx\UserAdminBundle\Entity\xxxUser")
     */
    private $account;

    /**
     * @ORM\Column(type="TransportStatus", length=20, nullable=false)
     * @DoctrineEnumAssert\Enum(entity="App\DBAL\Types\TransportStatusType")
     */
    private $transportStatus = 'NEW';

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\NotificationLog", mappedBy="notification")
     * @ORM\JoinColumn(nullable=true)
     */
    private $notificationLogs;

    /**
     * @ORM\Column(type="Transport")
     * @DoctrineEnumAssert\Enum(entity="xxx\NotificationAdminBundle\DBAL\Types\TransportType")
     */
    private $transport;

    /**
     * @ORM\Column(type="MessageType", length=20, nullable=false)
     * @DoctrineEnumAssert\Enum(entity="xxx\NotificationAdminBundle\DBAL\Types\MessageType")
     */
    private $messageType = 'PLAIN';

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\NotificationAttachment", mappedBy="notification", cascade={"persist"})
     */
    private $attachments;

    public function __construct()
    {
        $this->notificationLogs = new ArrayCollection();
        $this->attachments = new ArrayCollection();
    }

        public function getId(): ?int
    {
        return $this->id;
    }

    public function getItemId(): ?int
    {
        return $this->itemId;
    }

    public function setItemId(?int $itemId): self
    {
        $this->itemId = $itemId;

        return $this;
    }

    public function getRecipient(): ?string
    {
        return $this->recipient;
    }

    public function setRecipient(string $recipient): self
    {
        $this->recipient = $recipient;

        return $this;
    }

    public function getSubject(): ?string
    {
        return $this->subject;
    }

    public function setSubject(string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(?string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getEvent(): ?Event
    {
        return $this->event;
    }

    public function setEvent(?Event $event): self
    {
        $this->event = $event;

        return $this;
    }

    public function getAccount(): ?xxxUser
    {
        return $this->account;
    }

    public function setAccount(?xxxUser $account): self
    {
        $this->account = $account;

        return $this;
    }

    public function getTransportStatus()
    {
        return $this->transportStatus;
    }

    public function setTransportStatus($transportStatus): self
    {
        $this->transportStatus = $transportStatus;

        return $this;
    }

    /**
     * @return Collection|NotificationLog[]
     */
    public function getNotificationLogs(): Collection
    {
        return $this->notificationLogs;
    }

    public function addNotificationLog(NotificationLog $notificationLog): self
    {
        if (!$this->notificationLogs->contains($notificationLog)) {
            $this->notificationLogs[] = $notificationLog;
            $notificationLog->setNotification($this);
        }

        return $this;
    }

    public function removeNotificationLog(NotificationLog $notificationLog): self
    {
        if ($this->notificationLogs->contains($notificationLog)) {
            $this->notificationLogs->removeElement($notificationLog);
            // set the owning side to null (unless already changed)
            if ($notificationLog->getNotification() === $this) {
                $notificationLog->setNotification(null);
            }
        }

        return $this;
    }

    public function getTransport()
    {
        return $this->transport;
    }

    public function setTransport($transport): self
    {
        $this->transport = $transport;

        return $this;
    }

    public function getMessageType()
    {
        return $this->messageType;
    }

    public function setMessageType($messageType): self
    {
        $this->messageType = $messageType;

        return $this;
    }

    /**
     * @return Collection|NotificationAttachment[]
     */
    public function getAttachments(): Collection
    {
        return $this->attachments;
    }

    public function addNotificationAttachment(NotificationAttachment $attachment): self
    {
        if (!$this->attachments->contains($attachment)) {
            $this->attachments[] = $attachment;
            $attachment->setNotification($this);
        }

        return $this;
    }

    public function removeNotificationAttachment(NotificationAttachment $attachment): self
    {
        if ($this->attachments->contains($attachment)) {
            $this->attachments->removeElement($attachment);
            // set the owning side to null (unless already changed)
            if ($attachment->getNotification() === $this) {
                $attachment->setNotification(null);
            }
        }

        return $this;
    }
}
