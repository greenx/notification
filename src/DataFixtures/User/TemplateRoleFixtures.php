<?php
/**
 * Created by PhpStorm.
 * User: greenx
 * Date: 24.09.18
 * Time: 17:51
 */

namespace App\DataFixtures\User;

use xxx\UserAdminBundle\Entity\TemplateRole;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class TemplateRoleFixtures extends Fixture
{
    const TEMPLATE_ROLE = 'templateRole';

    public function load(ObjectManager $objectManager)
    {
        $templateRole = new TemplateRole();
        $templateRole->setName('Template Booker');

        $objectManager->persist($templateRole);
        $objectManager->flush();
        $this->addReference(self::TEMPLATE_ROLE, $templateRole);
    }
}