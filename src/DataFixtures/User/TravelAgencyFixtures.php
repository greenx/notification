<?php
/**
 * Created by PhpStorm.
 * User: greenx
 * Date: 24.09.18
 * Time: 17:51
 */

namespace App\DataFixtures\User;

use xxx\UserAdminBundle\Entity\TravelAgency;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class TravelAgencyFixtures extends Fixture
{
    public const TRAVEL_AGENCY = 'travelAgency';

    public function load(ObjectManager $objectManager)
    {
        $travelAgency = new TravelAgency();
        $travelAgency->setName('Travel Agency "Horns and hooves"');
        $objectManager->persist($travelAgency);
        $objectManager->flush();
        $this->addReference(self::TRAVEL_AGENCY, $travelAgency);
    }
}