<?php

namespace App\DataFixtures\User;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Faker\Factory;
use xxx\UserAdminBundle\Entity\xxxUser;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class xxxUserFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param ObjectManager $manager
     *
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        // Reset sequence
        /** @var Connection $connection */
        $connection = $this->em->getConnection();
        $connection->exec("ALTER SEQUENCE xxx_user.account_id_seq RESTART WITH 1;");
        $connection->exec("UPDATE xxx_user.account SET id=nextval('xxx_user.account_id_seq');");


        $faker = Factory::create(Factory::DEFAULT_LOCALE);

        for ($i = 1; $i <= 5; $i++) {
            $user = new xxxUser();
            $user->setName($faker->name);
            $user->setEmails([$faker->email]);
            $user->setLogin($faker->userName);
            $user->setPhone($faker->phoneNumber);
            $user->setAdditionalPhone($faker->phoneNumber);
            $user->setBirthday($faker->dateTime);
            $user->setPassword($faker->userName);

            /** @var Legal $legal */
            $legal = $this->getReference(LegalFixtures::LEGAL);
            $user->setLegal($legal);

            /** @var Role $role */
            $role = $this->getReference(RoleFixtures::$names->first());
            $user->setRole($role);


            $manager->persist($user);
            $this->addReference('user_' . $i, $user);
        }

        $manager->flush();
    }
    public function getDependencies()
    {
        return array(
            TravelAgencyFixtures::class,
            TemplateOrganizationFixtures::class
        );
    }
}