<?php
/**
 * Created by PhpStorm.
 * User: greenx
 * Date: 24.09.18
 * Time: 17:38
 */

namespace App\DataFixtures\User;

use xxx\UserAdminBundle\Entity\xxxUser;
use xxx\UserAdminBundle\Entity\Legal;
use xxx\UserAdminBundle\Entity\Role;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class AccountFixtures extends Fixture implements DependentFixtureInterface
{
    public const ACCOUNT = 'account';

    public const ACCOUNT_LOGIN = [
        'test',
        'test1',
        'test2',
        'test3',
        'test4',
        'test5',
        'dev',
        'dev1',
        'dev2',
        'dev3',
        'dev4',
        'dev5'
    ];

    /**
     * @param ObjectManager $objectManager
     * @throws \Exception
     */
    public function load(ObjectManager $objectManager)
    {
        foreach (self::ACCOUNT_LOGIN as $login) {
            $account = new xxxUser();
            /** @var Legal $legal */
            $legal = $this->getReference(LegalFixtures::LEGAL);
            $account->setLegal($legal);

            /** @var Role $role */
            $role = $this->getReference(RoleFixtures::$names->first());
            $account->setRole($role);

            $account->setName('Test');
            $account->setEmails([$login . '@xxx.ru']);
            $account->setPhone('+79295906845');
            $account->setAdditionalPhone('79295906846');
            $account->setSite('http://testsite.ru');
            $account->setBirthday(new \DateTime('1991-08-24'));
            $account->setAdditionalInfo('Дополнительная инфа какая то');
            $account->setLogin($login);
            $account->setPassword($login);

            $objectManager->persist($account);
            $objectManager->flush();
        }
    }

    public function getDependencies()
    {
        return array(
            LegalFixtures::class,
            RoleFixtures::class
        );
    }
}