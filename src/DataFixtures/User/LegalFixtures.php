<?php
/**
 * Created by PhpStorm.
 * User: greenx
 * Date: 24.09.18
 * Time: 17:38
 */

namespace App\DataFixtures\User;

use xxx\UserAdminBundle\Entity\Legal;
use xxx\UserAdminBundle\Entity\TemplateOrganization;
use xxx\UserAdminBundle\Entity\TravelAgency;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LegalFixtures extends Fixture implements DependentFixtureInterface
{
    public const LEGAL = 'legal';

    public function load(ObjectManager $objectManager)
    {
        $legal = new Legal();
        $legal->setName('Some travel agency');

        /** @var TemplateOrganization $templateOrganization */
        $templateOrganization = $this->getReference(TemplateOrganizationFixtures::TEMPLATE_ORGANIZATION);
        $legal->setTemplateOrganization($templateOrganization);

        /** @var TravelAgency $travelAgency */
        $travelAgency = $this->getReference(TravelAgencyFixtures::TRAVEL_AGENCY);
        $legal->setTravelAgency($travelAgency);

        $objectManager->persist($legal);
        $objectManager->flush();

        $this->addReference(self::LEGAL, $legal);
    }

    public function getDependencies()
    {
        return array(
            TravelAgencyFixtures::class,
            TemplateOrganizationFixtures::class
        );
    }
}