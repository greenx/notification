<?php
/**
 * Created by PhpStorm.
 * User: greenx
 * Date: 24.09.18
 * Time: 17:39
 */

namespace App\DataFixtures\User;

use xxx\UserAdminBundle\Entity\Legal;
use xxx\UserAdminBundle\Entity\Role;
use xxx\UserAdminBundle\Entity\TemplateRole;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class RoleFixtures
 * @package App\DataFixtures\xxxUser\Role
 */
class RoleFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * @var ArrayCollection
     */
    public static $names;

    /**
     * RoleFixtures constructor.
     */
    public function __construct()
    {
        self::$names = new ArrayCollection();
    }

    /**
     * @param ObjectManager $objectManager
     */
    public function load(ObjectManager $objectManager)
    {
        foreach (['Bookkeeper', 'Crazy bookkeeper'] as $name) {
            $role = new Role();
            $role->setName($name);

            /** @var Legal $legal */
            $legal = $this->getReference(LegalFixtures::LEGAL);
            $role->setLegal($legal);

            /** @var TemplateRole $templateRole */
            $templateRole = $this->getReference(TemplateRoleFixtures::TEMPLATE_ROLE);
            $role->setTemplateRole($templateRole);

            $objectManager->persist($role);
            $objectManager->flush();
            $this->addReference($name, $role);
            self::$names->add($name);
        }
    }

    /**
     * @return array
     */
    public function getDependencies()
    {
        return array(
            LegalFixtures::class,
            TemplateRoleFixtures::class
        );
    }
}