<?php
/**
 * Created by PhpStorm.
 * User: greenx
 * Date: 24.09.18
 * Time: 17:51
 */

namespace App\DataFixtures\User;

use xxx\UserAdminBundle\Entity\TemplateOrganization;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class TemplateOrganizationFixtures extends Fixture
{
    public const TEMPLATE_ORGANIZATION = 'templateOrganization';

    public function load(ObjectManager $objectManager)
    {
        $templateOrganization = new TemplateOrganization();
        $templateOrganization->setName('External users');
        $objectManager->persist($templateOrganization);
        $objectManager->flush();
        $this->addReference(self::TEMPLATE_ORGANIZATION, $templateOrganization);
    }
}