<?php

namespace App\DataFixtures;


use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use xxx\NotificationAdminBundle\Entity\Event;
use xxx\NotificationAdminBundle\Entity\EventRule;
use xxx\NotificationAdminBundle\Entity\RecipientInside;
use xxx\NotificationAdminBundle\Entity\RecipientRemote;
use xxx\NotificationAdminBundle\Entity\RecipientStatic;

class EventFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $recipientStatic = new RecipientStatic();
        $recipientStatic->setArgument('1');

        $recipientRemote = new RecipientRemote();
        $recipientRemote->setArgument('https://yandex.ru/');

        $recipientInside = new RecipientInside();
        $recipientInside->setArgument('accountId');



        $rule = new EventRule();
        $rule->setArgument('msg.accountId == 20');

        $rule2 = new EventRule();
        $rule2->setArgument('msg.id==662 && msg.providerId==123');


        $event = new Event();
        $event->setName('Заказ содан - Куратор')
            ->setEvent('BookingHotelCreated')
            ->setItemIdTemplate('id')
            ->setTransport('MAIL')
            ->setApplication('HOTEL')
            ->setMessageType('HTML')
            ->setIsInternal(false);

        $event->translate('ru')->setSubject('Новый заказ {{ orderId  }}');
        $event->translate('ru')->setMessage('
                Здравствуйте.
                Создан новый Заказ № {{ orderId }}.
                Дата/время: {{ createdDate}}
                ФИ менеджера: {{ curatorId }}
                Клиент: {{ accountId }} 
                
                ЗАКАЗ ОЧ: {% if online == false %}ДА{% else %}НЕТ{% endif %}
                ШТРАФ: {% if online == false %}ДА{% else %}НЕТ{% endif %}
                IHG, MARRIOT, EXP HC: {% if online == false %}ДА{% else %}НЕТ{% endif %}
                ЗАПРОС ЗАРУБЕЖ: {% if online == false %}ДА{% else %}НЕТ{% endif %}
                АЭРОФЛОТ: {% if online == false %}ДА{% else %}НЕТ{% endif %}');
        $event->translate('en')->setSubject('New order {{ order.item.id }}');
        $event->translate('en')->setMessage('
                Hello.
                 New order number {{id}} has been created.
                 Date / Time: Date and time the order was created
                 FI manager (aka reference): Transfer field value
                 Client: Name TA, name jur. persons (if there are several TA)
                
                 ORDER ORDER: YES / NO
                 FINE: YES / NO
                 IHG, MARRIOT, EXP HC: YES / NO
                 REQUEST FOREIGN: YES / NO
                 AEROFLOT: YES / NO');


        $event->mergeNewTranslations();
        $event->addRecipient($recipientInside);
        $event->addRecipient($recipientRemote);
        $event->addRecipient($recipientStatic);

        $event->addRule($rule);
        $event->addRule($rule2);
        $manager->persist($event);

        $manager->flush();
    }
}
