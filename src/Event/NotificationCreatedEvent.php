<?php

namespace App\Event;
use App\Entity\Notification;
use Symfony\Component\EventDispatcher\Event;

class NotificationCreatedEvent extends Event
{
    const NAME = 'notification.created';

    /**
     * @var Notification
     */
    private $notification;

    /**
     * NotificationCreatedEvent constructor.
     * @param Notification $notification
     */
    public function __construct(Notification $notification)
    {
        $this->notification = $notification;
    }

    /**
     * @return Notification
     */
    public function getNotification(): Notification
    {
        return $this->notification;
    }
}